export class UpstreamFetchError extends Error {}
export class TopologyFetchError extends Error {}
export class InvalidUrlSafeBase64 extends Error {}
export class InvalidTokenPayloadError extends Error {}
export class MaxAttemptsReached extends Error {}
export class NotFound extends Error {}
