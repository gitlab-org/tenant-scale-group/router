export type LogLevel = 'info' | 'debug';

export class Logger {
  private static instance: Logger = new Logger();
  public static getInstance(): Logger {
    return this.instance;
  }

  logLevel: LogLevel;
  device: Console;

  constructor(logLevel: LogLevel = 'info', device: Console = console) {
    this.logLevel = logLevel;
    this.device = device;
  }

  public updateLogLevel(logLevel: Env['LOGGING_LEVEL']): void {
    this.logLevel = logLevel === 'debug' ? 'debug' : 'info';
  }

  /* eslint-disable @typescript-eslint/no-explicit-any */
  public info(...data: any[]) {
    this.device.log(...JSON.parse(JSON.stringify(data)));
  }

  public debug(...data: any[]) {
    if (this.logLevel == 'debug') {
      this.device.log(...JSON.parse(JSON.stringify(data)));
    }
  }
  /* eslint-enable @typescript-eslint/no-explicit-any */
}

export function buildSilentLogger(logLevel: LogLevel = 'info'): Logger {
  return new Logger(logLevel, { log: () => {} } as Console);
}
