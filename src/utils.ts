import { InvalidUrlSafeBase64 } from './error';

export function encodeUrlSafeBase64(input: string): string {
  return btoa(input).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
}

export function decodeUrlSafeBase64(urlSafeBase64: string): string {
  const base64 = urlSafeBase64.replace(/-/g, '+').replace(/_/g, '/');
  try {
    return atob(base64);
  } catch {
    throw new InvalidUrlSafeBase64(`Invalid URL safe base64: ${urlSafeBase64.slice(0, 10)}...`);
  }
}

export function base36ToBigInt(input: string): bigint {
  return input.split('').reduce((result, char) => result * 36n + BigInt(parseInt(char, 36)), 0n);
}

export async function sha256(message: string) {
  const msgBuffer = new TextEncoder().encode(message);
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
  return [...new Uint8Array(hashBuffer)].map((b) => b.toString(16).padStart(2, '0')).join('');
}

export async function measureTime<Result>(callback: () => Promise<Result>): Promise<{ duration: number; result: Result }> {
  const startTime = performance.now();
  try {
    const result = await callback();
    return { duration: performance.now() - startTime, result };
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any -- How otherwise can we attach the duration? Actually, I think we should wrap the exception, rather than extending it */
  } catch (error: any) {
    const duration = performance.now() - startTime;
    throw Object.assign(error, { duration });
  }
}

export function catchFetchError<FetchError extends Error>(FetchErrorConstructor: new (message: string) => FetchError) {
  return (error: Error): never => {
    if (error.message === 'Network connection lost.') {
      // error from fetch:
      // https://gitlab.com/gitlab-org/cells/http-router/-/issues/112
      throw new FetchErrorConstructor(error.message);
    } else if (error.name === 'TimeoutError') {
      /* error from AbortSignal.timeout
       https://developer.mozilla.org/en-US/docs/Web/API/AbortSignal/timeout_static
       https://developers.cloudflare.com/workers/runtime-apis/web-standards/#abortcontroller-and-abortsignal */
      throw new FetchErrorConstructor(error.message);
    } else {
      throw error;
    }
  };
}

export async function sleepPromise(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
