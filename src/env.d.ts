interface Env {
  GITLAB_PROXY_HOST: string | undefined;
  GITLAB_TOPOLOGY_SERVICE_URL: string | URL | undefined;
  GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS: number | undefined;
  GITLAB_RULES_CONFIG: string | undefined;
  GITLAB_ENV: string | undefined;
  LOGGING_LEVEL: string | undefined;
  SENTRY_DSN: string | undefined;
  CF_VERSION_METADATA: WorkerVersionMetadata | undefined;
}
