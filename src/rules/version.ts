import { Rule } from './rule';
import { VersionRuleInfo, MatchResult } from './types';

export class VersionRule extends Rule<VersionRuleInfo> {
  private static readonly contentType = { 'Content-Type': 'application/json' };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars -- matchResult and ctx is not used but it's part of the method signature
  override async fetch(matchResult: MatchResult, request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    const versionInfo = {
      id: env.CF_VERSION_METADATA?.id ?? 'dev',
      tag: env.CF_VERSION_METADATA?.tag ?? 'dev',
    };

    this.logger.info({ versionInfo });

    return new Response(JSON.stringify(versionInfo), {
      headers: VersionRule.contentType,
    });
  }
}
