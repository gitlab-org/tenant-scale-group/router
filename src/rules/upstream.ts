import { catchFetchError } from '../utils';
import { UpstreamFetchError } from '../error';

export class Upstream {
  private request: Request;

  constructor(originalRequest: Request, host: string) {
    const url = new URL(originalRequest.url);
    const originalHost = url.host;
    url.host = host;

    // Best practice is to always use the original request to construct the new request
    // to clone all the attributes. Applying the URL also requires a constructor
    // since once a Request has been constructed, its URL is immutable.
    this.request = new Request(url.toString(), originalRequest);

    // Set this to enable the application to generate correct links
    this.request.headers.set('X-Forwarded-Host', originalHost);
  }

  public getRequest(): Request {
    return this.request;
  }

  public async fetch(): Promise<Response> {
    return fetch(this.request).catch(catchFetchError(UpstreamFetchError));
  }
}
