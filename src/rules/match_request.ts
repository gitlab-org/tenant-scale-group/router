import { RegexMatchInfo, MaybeMatchResult, MatchInfo, HeaderMatchInfo, MatchType } from './types.d';

export function getCookieValue(name: string, request: Request): string | null {
  const cookie = request.headers.get('Cookie') ?? '';
  const values = cookie.match(new RegExp(`(?<=(^| )${name}=)[-_\\w]+`));
  return values ? values[0] : null;
}

export function getQueryValue(name: string, url: URL): string | null {
  const searchParams = url.searchParams;
  const regex = new RegExp(`^${name}$`, 'i');
  for (const [key, value] of searchParams.entries()) {
    if (regex.test(key)) {
      return value;
    }
  }
  return null;
}

function matchRegex(value: string, regexValue: string): MaybeMatchResult {
  const regex = new RegExp(regexValue);
  const match = value.match(regex);
  return match ? (match.groups ?? {}) : null;
}

export function matchCookie(matchInfo: RegexMatchInfo, request: Request): MaybeMatchResult {
  const cookieValue = getCookieValue(matchInfo.regex_name, request);
  return cookieValue ? matchRegex(cookieValue, matchInfo.regex_value) : null;
}

export function matchHeader(matchInfo: HeaderMatchInfo, request: Request): MaybeMatchResult {
  const headerValue = request.headers.get(matchInfo.name) ?? '';
  return matchRegex(headerValue, matchInfo.regex_value);
}

export function matchQueryString(matchInfo: RegexMatchInfo, url: URL): MaybeMatchResult {
  const queryValue = getQueryValue(matchInfo.regex_name, url);
  return queryValue ? matchRegex(queryValue, matchInfo.regex_value) : null;
}

export function matchPath(matchInfo: RegexMatchInfo, url: URL): MaybeMatchResult {
  return matchRegex(url.pathname, matchInfo.regex_value);
}

export function matchRequest(matchInfo: MatchInfo, request: Request): MaybeMatchResult {
  switch (matchInfo.type) {
    case MatchType.Cookie:
      return matchCookie(matchInfo as RegexMatchInfo, request);
    case MatchType.Header:
      return matchHeader(matchInfo as HeaderMatchInfo, request);
    case MatchType.QueryString:
      return matchQueryString(matchInfo as RegexMatchInfo, new URL(request.url));
    case MatchType.Path:
      return matchPath(matchInfo as RegexMatchInfo, new URL(request.url));
    default:
      throw new Error(`Unknown match type: ${matchInfo.type}`);
  }
}
