export enum ActionType {
  Classify = 'classify',
  Proxy = 'proxy',
  Version = 'version',
}

export enum ClassifyType {
  SessionPrefix = 'SESSION_PREFIX',
  FirstCell = 'FIRST_CELL',
  CellID = 'CELL_ID',
}

export enum MatchType {
  Header = 'header',
  QueryString = 'query_string',
  Path = 'path',
  Cookie = 'cookie',
}

interface BaseMatchInfo {
  type: MatchType;
}

interface RegexMatchInfo extends BaseMatchInfo {
  regex_name: string;
  regex_value: string;
}

interface HeaderMatchInfo extends BaseMatchInfo {
  name: string;
  regex_value: string;
}

type MatchInfo = RegexMatchInfo | HeaderMatchInfo;
type TransformType = 'routable-token-payload' | 'routable-json';
type TransformInput = [string] | [string, string];

interface TransformInfo {
  type: TransformType;
  input: TransformInput;
  output: string;
}

interface ValidateInfo {
  exist: TransformInput;
}

interface BaseRuleInfo {
  id: string;
  match?: MatchInfo;
  transform?: TransformInfo;
  validate?: ValidateInfo;
}

interface ClassifyRuleInfo extends BaseRuleInfo {
  action: ActionType.Classify;
  classify: {
    type: ClassifyType;
    value?: string;
  };
}

interface ProxyRuleInfo extends BaseRuleInfo {
  action: ActionType.Proxy;
  proxy?: {
    address: string;
  };
}

interface VersionRuleInfo extends BaseRuleInfo {
  action: ActionType.Version;
}

export type RuleInfo = ClassifyRuleInfo | ProxyRuleInfo | VersionRuleInfo;
export type RulesInfo = RuleInfo[];
export type MatchResult = { [key: string]: string };
export type MaybeMatchResult = MatchResult | null;
