import { RulesInfo, ActionType, RuleInfo } from './types.d';
import { Rule } from './rule';
import { ClassifyRule } from './classify';
import { ProxyRule } from './proxy';
import { VersionRule } from './version';
import { Logger } from '../logger';

import session_prefix from '../../config/ruleset/session_prefix.json'; // docs/rules/session_prefix.md
import session_token from '../../config/ruleset/session_token.json';
import passthrough from '../../config/ruleset/passthrough.json';
import firstcell from '../../config/ruleset/firstcell.json';

const allRulesInfo: Record<string, RulesInfo> = {
  session_prefix: session_prefix as RulesInfo,
  session_token: session_token as RulesInfo,
  passthrough: passthrough as RulesInfo,
  firstcell: firstcell as RulesInfo,
};

export function buildRules(rulesConfig: string, logger: Logger): Rule[] {
  const rulesInfo = allRulesInfo[rulesConfig];
  if (!rulesInfo) {
    throw new Error(`The '${rulesConfig}' was not found.`);
  }

  return buildRulesFromRulesInfo(rulesInfo, logger);
}

export function buildRulesFromRulesInfo(rulesInfo: RulesInfo, logger: Logger): Rule[] {
  return rulesInfo.map((ruleInfo: RuleInfo) => {
    switch (ruleInfo.action) {
      case ActionType.Classify:
        return new ClassifyRule(ruleInfo, logger);
      case ActionType.Proxy:
        return new ProxyRule(ruleInfo, logger);
      case ActionType.Version:
        return new VersionRule(ruleInfo, logger);
      default:
        throw new Error(`Unsupported rule action`);
    }
  });
}
