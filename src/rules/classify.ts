import { Rule } from './rule';
import { ROUTER_RULE_TYPE_HEADER } from './constants';
import { ClassifyRuleInfo, MatchResult } from './types.d';
import { classifyFetch } from '../topology_service/classify';
import { ClassifyAction, ClassifyResponse } from '../topology_service/types.d';

export class ClassifyRule extends Rule<ClassifyRuleInfo> {
  override setHeaders(request: Request): void {
    super.setHeaders(request);
    request.headers.set(ROUTER_RULE_TYPE_HEADER, this.ruleInfo.classify.type);
  }

  override async fetch(matchResult: MatchResult, request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    if (!env.GITLAB_TOPOLOGY_SERVICE_URL) {
      throw new Error('Topology Service is not configured to perform classify action');
    }

    const { type, value } = this.ruleInfo.classify;
    const interpolatedValue = this.interpolateMaybeValue(matchResult, value);
    const response = await classifyFetch(env, ctx, this.logger, type, interpolatedValue);
    return this.handleClassifyResponse(request, response);
  }

  protected interpolateMaybeValue(matchResult: MatchResult, value?: string): string | undefined {
    return value ? this.interpolateValue(matchResult, value) : undefined;
  }

  private handleClassifyResponse(request: Request, response: ClassifyResponse): Promise<Response> {
    switch (response.action) {
      case ClassifyAction.Proxy:
        return this.fetchUpstream(request, response.proxy.address);
      default:
        throw new Error(`Topology Service action is not supported: ${response.action}`);
    }
  }
}
