export const ROUTER_RULE_ACTION_HEADER = 'X-GitLab-HTTP-Router-Rule-Action';
export const ROUTER_RULE_TYPE_HEADER = 'X-GitLab-HTTP-Router-Rule-Type';
export const RULE_VARIABLE_PATTERN = /\$\{([\w.]+)\}/g;
export const ROUTING_KEYS = ['c', 'g', 'o', 'p', 'u'];
