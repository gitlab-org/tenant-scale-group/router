import { Rule } from './rule';
import { ProxyRuleInfo, MatchResult } from './types.d';

export class ProxyRule extends Rule<ProxyRuleInfo> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars -- matchResult and ctx is not used but it's part of the method signature
  override async fetch(matchResult: MatchResult, request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    const proxyAddress = this.ruleInfo.proxy?.address ?? env.GITLAB_PROXY_HOST;

    if (!proxyAddress) {
      throw new Error('Proxy address is not specified in rule or environment.');
    }

    return this.fetchUpstream(request, proxyAddress);
  }
}
