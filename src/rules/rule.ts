import { ROUTER_RULE_ACTION_HEADER, RULE_VARIABLE_PATTERN, ROUTING_KEYS } from './constants';
import { RuleInfo, MatchResult, MaybeMatchResult } from './types.d';
import { matchRequest } from './match_request';
import { Upstream } from './upstream';
import { base36ToBigInt, decodeUrlSafeBase64 } from '../utils';
import { Logger } from '../logger';
import { InvalidTokenPayloadError } from '../error';

export abstract class Rule<T extends RuleInfo = RuleInfo> {
  constructor(
    protected ruleInfo: T,
    protected logger: Logger,
  ) {}

  match(request: Request): MaybeMatchResult {
    if (!this.ruleInfo.match) return {};

    const matchResult = matchRequest(this.ruleInfo.match, request);
    if (!matchResult) return null;

    const transformedResult = this.transformMatchResult(matchResult);
    return this.isMatchResultValid(transformedResult) ? transformedResult : null;
  }

  async apply(matchResult: MatchResult, request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    this.logger.info({ ruleApplied: this.ruleInfo.id, ruleAction: this.ruleInfo.action });
    return this.fetch(matchResult, request, env, ctx);
  }

  protected async fetchUpstream(request: Request, host: string): Promise<Response> {
    const upstream = new Upstream(request, host);
    this.setHeaders(upstream.getRequest());

    this.logger.info({ proxyHost: host });

    return upstream.fetch();
  }

  protected setHeaders(request: Request): void {
    request.headers.set(ROUTER_RULE_ACTION_HEADER, this.ruleInfo.action);
  }

  // This interpolates "${cell_name}" with the value in the capture group of
  // /(?<cell_name>cell-\d+)/.
  // `matchResult` can look like: `{ cell_name: 'cell-123' }` in this case.
  protected interpolateValue(matchResult: MatchResult, value: string): string {
    return value.replace(RULE_VARIABLE_PATTERN, (_, name) => matchResult[name] ?? '');
  }

  protected abstract fetch(matchResult: MatchResult, request: Request, env: Env, ctx: ExecutionContext): Promise<Response>;

  private transformMatchResult(matchResult: MatchResult): MatchResult {
    const transformInfo = this.ruleInfo.transform;

    if (!transformInfo) return matchResult;

    const inputs = transformInfo.input.map((argument) => {
      return this.interpolateValue(matchResult, argument);
    });

    const transformResult = this.transformFromType(transformInfo.type, transformInfo.output, inputs);
    return { ...matchResult, ...transformResult };
  }

  private isMatchResultValid(matchResult: MatchResult): boolean {
    const validateInfo = this.ruleInfo.validate;
    if (!validateInfo) return true;

    return validateInfo.exist.every((argument) => this.interpolateValue(matchResult, argument) !== '');
  }

  private transformFromType(type: string, output: string, inputs: string[]): MatchResult {
    switch (type) {
      case 'routable-token-payload':
        return this.decodeRoutableTokenPayload(output, inputs[0], inputs[1]);
      case 'routable-json':
        return this.decodeRoutableJson(output, inputs[0]);
      default:
        throw new Error(`Unknown transform type: ${type}`);
    }
  }

  private decodeRoutableTokenPayload(prefix: string, payload: string, length: string): MatchResult {
    // length is small enough that parseInt is enough. The rules already
    // make sure it must be [0-9a-z]{2}
    const encoded = payload.slice(-parseInt(length, 36));
    const decoded = decodeUrlSafeBase64(encoded);
    const randomBytesSize = decoded[decoded.length - 1].charCodeAt(0);
    const payloadPairs = decoded.slice(0, decoded.length - randomBytesSize - 1).split('\n');

    return payloadPairs.reduce((result, pair) => {
      const { key, value } = this.parseKeyValuePair(pair);

      if (!ROUTING_KEYS.includes(key)) return result;

      result[`${prefix}.${key}`] = this.decodeBase36Value(value).toString();

      return result;
    }, {} as MatchResult);
  }

  private decodeRoutableJson(prefix: string, encoded: string): MatchResult {
    const payloadPairs = JSON.parse(decodeUrlSafeBase64(encoded));

    return Object.entries(payloadPairs).reduce((result, [key, value]) => {
      if (key === '' || value === '') {
        throw new InvalidTokenPayloadError(`Invalid pair: key or value missing in ${key}:${value}`);
      }

      if (!ROUTING_KEYS.includes(key)) return result;

      result[`${prefix}.${key}`] = this.decodeBase36ValueFromUnknown(value).toString();
      return result;
    }, {} as MatchResult);
  }

  private parseKeyValuePair(pair: string): { key: string; value: string } {
    const splitAt = pair.indexOf(':');
    if (splitAt === -1) {
      throw new InvalidTokenPayloadError(`Invalid pair: missing separator ":" in ${pair}`);
    }

    const key = pair.slice(0, splitAt);
    const value = pair.slice(splitAt + 1);

    if (key === '' || value === '') {
      throw new InvalidTokenPayloadError(`Invalid pair: key or value missing in ${pair}`);
    }

    return { key, value };
  }

  private decodeBase36ValueFromUnknown(value: unknown): bigint {
    if (typeof value === 'string') {
      return this.decodeBase36Value(value);
    } else {
      throw new InvalidTokenPayloadError(`Invalid base36 type: ${typeof value}`);
    }
  }

  private decodeBase36Value(value: string): bigint {
    if (/^[0-9a-z]+$/.test(value)) {
      return base36ToBigInt(value);
    } else {
      throw new InvalidTokenPayloadError(`Invalid base36 value: ${value}`);
    }
  }
}
