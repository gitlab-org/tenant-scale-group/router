import { buildRules } from './rules/index';
import { Logger } from './logger';
import * as Sentry from '@sentry/cloudflare';
import { RouterEngine } from './router_engine';

export default Sentry.withSentry(
  (env: Env) => ({
    dsn: env.SENTRY_DSN,
    environment: env.GITLAB_ENV,
  }),
  {
    async fetch(request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
      if (!env.GITLAB_RULES_CONFIG) {
        throw new Error('The GITLAB_RULES_CONFIG is not configured.');
      }

      const logger = Logger.getInstance();
      logger.updateLogLevel(env.LOGGING_LEVEL);

      const rules = buildRules(env.GITLAB_RULES_CONFIG, logger);
      const routingEngine = new RouterEngine(rules, env);
      return routingEngine.fetch(request, ctx, logger).catch(routingEngine.handleError);
    },
  } satisfies ExportedHandler<Env>,
);
