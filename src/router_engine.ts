import { Logger } from './logger';
import { Rule } from './rules/rule';
import * as errors from './error';

export class RouterEngine {
  private readonly rules: Rule[];
  private readonly env: Env;

  constructor(rules: Rule[], env: Env) {
    this.rules = rules;
    this.env = env;
  }

  async fetch(request: Request, ctx: ExecutionContext, logger: Logger): Promise<Response> {
    for (const rule of this.rules) {
      const matchResult = rule.match(request);
      if (matchResult) return rule.apply(matchResult, request, this.env, ctx);
    }

    logger.info({ msg: 'No rules matched' });
    throw new Error('No rules matched.');
  }

  handleError(error: Error): Response {
    const defaultHeaders = { 'Content-Type': 'text/plain' };
    if (error instanceof errors.UpstreamFetchError) {
      return new Response('502 Bad Gateway', { status: 502, headers: defaultHeaders });
    } else if (error instanceof errors.MaxAttemptsReached) {
      return new Response('503 Service Unavailable', { status: 503, headers: defaultHeaders });
    } else if (error instanceof errors.InvalidUrlSafeBase64) {
      return new Response('400 Bad Request', { status: 400, headers: defaultHeaders });
    } else if (error instanceof errors.InvalidTokenPayloadError) {
      return new Response('400 Bad Request', { status: 400, headers: defaultHeaders });
    } else if (error instanceof errors.NotFound) {
      return new Response('404 Not Found', { status: 404, headers: defaultHeaders });
    } else {
      throw error; // unknown error
    }
  }
}
