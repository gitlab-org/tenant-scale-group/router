import { Logger } from '../logger';
import { CLASSIFY_PATH, CLASSIFY_USER_AGENT, DEFAULT_TIMEOUT_MS } from './constants';
import { fetchWithRetry, secureCachedApiFetch } from './fetch';
import { ClassifyRequest, ClassifyResponse } from './types';
import { NotFound } from '../error';

export class TopologyServiceClient {
  private readonly url: URL;
  private readonly timeoutMs: number;

  constructor(
    private readonly env: Env,
    private readonly logger: Logger,
  ) {
    this.url = new URL(`${this.env.GITLAB_TOPOLOGY_SERVICE_URL}${CLASSIFY_PATH}`);
    this.timeoutMs = this.topologyServiceTimeoutMs();
  }

  async classify(request: ClassifyRequest, ctx: ExecutionContext): Promise<ClassifyResponse> {
    const response = await this.makeRequest(this.url, request, ctx);
    await this.handleErrorResponse(response);
    return response.json();
  }

  private topologyServiceTimeoutMs(): number {
    const timeout = this.env.GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS ?? DEFAULT_TIMEOUT_MS;
    return timeout > 0 ? timeout : DEFAULT_TIMEOUT_MS;
  }

  private async makeRequest(url: URL, data: ClassifyRequest, ctx: ExecutionContext): Promise<Response> {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': CLASSIFY_USER_AGENT,
      },
      body: JSON.stringify(data),
      signal: AbortSignal.timeout(this.timeoutMs),
    };

    return fetchWithRetry(() => secureCachedApiFetch(ctx, this.logger, url.toString(), requestOptions));
  }

  private async handleErrorResponse(response: Response): Promise<void> {
    if (!response.ok) {
      switch (true) {
        case response.status === 404:
          throw new NotFound(`Client Error: ${response.statusText}`);
        case response.status >= 400 && response.status < 500:
          throw new Error(`Client Error: ${response.statusText}`);
        case response.status >= 500:
          throw new Error('Topology Service is not reachable.');
        default:
          throw new Error(`Service is not reachable. ${response.statusText}`);
      }
    }
  }
}
