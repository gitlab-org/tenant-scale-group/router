import { MaxAttemptsReached, TopologyFetchError } from '../error';
import { catchFetchError, measureTime, sha256, sleepPromise } from '../utils';
import { Logger } from '../logger';
import { RETRYABLE_STATUS_CODES } from './constants';

interface FetchResponse {
  response: Response;
  retryable: boolean;
}

interface FetchError {
  error: unknown;
  errorInfo: string;
  rethrowError: boolean;
}

type FetchResult = FetchResponse | FetchError;

// Cache POST requests https://developers.cloudflare.com/workers/examples/cache-post-request/
// This method prevents unencrypted values from being pushed to Cloudflare edge caching.
export async function secureCachedApiFetch(ctx: ExecutionContext, logger: Logger, url: string, options: RequestInit): Promise<Response> {
  // Caching based on https://developers.cloudflare.com/workers/reference/how-the-cache-works/#cache-api
  const cache = caches.default;

  let bodyHash = '';
  if (options.body) {
    bodyHash = await sha256(options.body.toString());
  }

  const cacheUrl = new URL(url);
  cacheUrl.searchParams.append('requestMethod', options.method ?? 'GET');
  cacheUrl.searchParams.append('bodyHash', bodyHash);
  const cacheKey = new Request(cacheUrl.toString(), {
    headers: options.headers,
    method: 'GET',
  });

  const cacheResult: { duration: number; result: Response | undefined } = await measureTime((): Promise<Response | undefined> => {
    return cache.match(cacheKey);
  });
  let response: Response | undefined = cacheResult.result;
  const duration = cacheResult.duration;
  let cacheHitOrMiss: string;

  if (response) {
    cacheHitOrMiss = 'hit';
  } else {
    cacheHitOrMiss = 'miss';
    const sourceResult: { duration: number; result: Response } = await measureTime((): Promise<Response> => {
      return fetch(url, options).catch(catchFetchError(TopologyFetchError));
    });
    response = sourceResult.result;
    ctx.waitUntil(cache.put(cacheKey, response.clone()));
  }

  logger.info({ source: url, cache: cacheHitOrMiss, cacheFetchDurationMs: duration });

  return response;
}

/* The worst-case scenario for the default behavior:
  1. First request has no delay
  2. Second request has a delay of around 20ms with Jitter.
  3. Last request has a delay of 40ms with Jitter.
*/
export async function fetchWithRetry(
  fetchFn: () => Promise<Response>,
  initialDelayInMS: number = 20,
  exponentialRate: number = 2,
  maxAttempts: number = 3,
  retryableStatusCodes: number[] = RETRYABLE_STATUS_CODES,
  retryableErrors: Array<new (message: string) => Error> = [TopologyFetchError],
): Promise<Response> {
  const logger = Logger.getInstance();

  for (let attempts = 1; attempts <= maxAttempts; ++attempts) {
    const { result, duration } = await measureTime<FetchResult>(() => {
      return fetchOnce(fetchFn, retryableStatusCodes, retryableErrors);
    });

    let nextDelay;
    const fetchResponse = result as FetchResponse;
    const fetchError = result as FetchError;

    // If we will retry, set nextDelay
    if (attempts < maxAttempts && (fetchResponse.retryable ?? fetchError.error)) {
      nextDelay = initialDelayInMS * Math.pow(exponentialRate, attempts - 1) * Math.random();
    }

    logger.info({
      source: fetchResponse.response?.url,
      fetchStatus: fetchResponse.response?.status,
      fetchDurationMs: duration,
      attempt: attempts,
      error: fetchError.errorInfo,
      nextDelay: nextDelay,
    });

    // We have a response that we don't want to retry
    if (fetchResponse.response && !fetchResponse.retryable) {
      return fetchResponse.response;
      // We have an error which we don't want to retry so we rethrow it
    } else if (fetchError.rethrowError) {
      throw fetchError.error;
      // We want to retry, so we sleep for the next delay and go on
    } else if (nextDelay) {
      await sleepPromise(nextDelay);
    }
  }

  logger.info({ msg: 'Max attempts reached', maxAttempts: maxAttempts });
  throw new MaxAttemptsReached(`Still failing after ${maxAttempts} attempts.`);
}

async function fetchOnce(
  fetchFn: () => Promise<Response>,
  retryableStatusCodes: number[],
  retryableErrors: Array<new (message: string) => Error>,
): Promise<FetchResult> {
  try {
    const response = await fetchFn();
    const retryable = retryableStatusCodes.includes(response.status);

    return { response, retryable };
  } catch (error: unknown) {
    const errorInfo = error instanceof Error ? error.message : String(error);
    const isRetryableError = retryableErrors.some((Err) => error instanceof Err);

    return { error, errorInfo, rethrowError: !isRetryableError };
  }
}
