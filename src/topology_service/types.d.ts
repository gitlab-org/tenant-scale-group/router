export interface ClassifyRequest {
  type: string;
  value?: string;
}

export enum ClassifyAction {
  Proxy = 'PROXY',
}

interface ClassifyResponse {
  action: ClassifyAction;
  proxy: {
    address: string;
  };
}
