export const CLASSIFY_PATH = '/v1/classify';
export const CLASSIFY_USER_AGENT = 'http-router';
export const RETRYABLE_STATUS_CODES = [500, 502, 503, 504];
export const DEFAULT_TIMEOUT_MS = 1000;
