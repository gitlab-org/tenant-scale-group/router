import { ClassifyResponse } from './types';
import { Logger } from '../logger';
import { TopologyServiceClient } from './client';

export async function classifyFetch(
  env: Env,
  ctx: ExecutionContext,
  logger: Logger,
  type: string,
  value?: string,
): Promise<ClassifyResponse> {
  const client = new TopologyServiceClient(env, logger);
  return client.classify({ type, value }, ctx);
}
