import { describe, it, expect, vi, test } from 'vitest';
import { createExecutionContext, env } from 'cloudflare:test';
import { RouterEngine } from './../src/router_engine';
import { buildSilentLogger, Logger } from '../src/logger';
import { Rule } from '../src/rules/rule';
import * as errors from './../src/error';

function createMockRule(matchReturnValue: object | null, applyReturnValue?: Response): Rule {
  return {
    match: vi.fn().mockReturnValue(matchReturnValue),
    apply: vi.fn().mockResolvedValue(applyReturnValue || new Response()),
  } as unknown as Rule;
}

describe('RouterEngine', () => {
  let rules: Rule[];
  const ctx = createExecutionContext();
  const logger = buildSilentLogger();
  let routerEngine: RouterEngine;

  describe('fetch', () => {
    const request = new Request('https://example.com');
    const matchResult = { matched: true };
    const expectedResponse = new Response('Success');

    it('should apply the first matching rule', async () => {
      const rule1 = createMockRule(null);
      const rule2 = createMockRule(matchResult, expectedResponse);

      rules = [rule1, rule2];
      routerEngine = new RouterEngine(rules, env);

      const result = await routerEngine.fetch(request, ctx, logger);

      expect(rule1.match).toHaveBeenCalledWith(request);
      expect(rule2.match).toHaveBeenCalledWith(request);
      expect(rule2.apply).toHaveBeenCalledWith(matchResult, request, env, ctx);
      expect(result).toBe(expectedResponse);
    });

    it('should throw an error when no rules match', async () => {
      const rule = createMockRule(null);
      const logger = Logger.getInstance();
      const logSpy = vi.spyOn(logger, 'info');

      rules = [rule];
      routerEngine = new RouterEngine(rules, env);

      await expect(routerEngine.fetch(request, ctx, logger)).rejects.toThrow('No rules matched.');
      expect(logSpy).toHaveBeenCalledExactlyOnceWith({ msg: 'No rules matched' });
    });

    it('should check rules in order and stop at first match', async () => {
      const rule1 = createMockRule(matchResult, expectedResponse);
      const rule2 = createMockRule(null);

      rules = [rule1, rule2];
      routerEngine = new RouterEngine(rules, env);

      await routerEngine.fetch(request, ctx, logger);

      expect(rule1.match).toHaveBeenCalledWith(request);
      expect(rule2.match).not.toHaveBeenCalled();
    });
  });

  describe('handleError', () => {
    routerEngine = new RouterEngine(rules, env);

    const errorCases = [
      { error: new errors.UpstreamFetchError('Upstream error'), expectedStatus: 502 },
      { error: new errors.MaxAttemptsReached('Max attempts reached'), expectedStatus: 503 },
      { error: new errors.InvalidUrlSafeBase64('Invalid base64'), expectedStatus: 400 },
      { error: new errors.InvalidTokenPayloadError('Invalid token'), expectedStatus: 400 },
      { error: new errors.NotFound('Resource not found'), expectedStatus: 404 },
    ];

    test.each(errorCases)('should return $expectedStatus for $error.constructor.name', ({ error, expectedStatus }) => {
      const response = routerEngine.handleError(error);

      expect(response.status).toBe(expectedStatus);
      expect(response.headers.get('Content-Type')).toBe('text/plain');
    });

    it('should re-throw unknown errors', () => {
      const error = new Error('Unknown error');

      expect(() => routerEngine.handleError(error)).toThrow('Unknown error');
    });
  });
});
