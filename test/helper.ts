import { env, createExecutionContext, waitOnExecutionContext, fetchMock } from 'cloudflare:test';
import { beforeEach, afterEach, vi, expect } from 'vitest';
import { RulesInfo, ClassifyType } from '../src/rules/types.d';
import { buildSilentLogger, Logger } from '../src/logger';
import { buildRulesFromRulesInfo } from '../src/rules/index';
import { RouterEngine } from '../src/router_engine';
import { encodeUrlSafeBase64 } from '../src/utils';
import { CLASSIFY_PATH } from '../src/topology_service/constants';
import worker from '../src';

export function withRulesConfig(value: string) {
  beforeEach(() => {
    env.GITLAB_RULES_CONFIG = value;
  });
  afterEach(() => {
    env.GITLAB_RULES_CONFIG = undefined;
  });
}

export function withProxyHost(host: string) {
  beforeEach(() => {
    env.GITLAB_PROXY_HOST = host;
  });
  afterEach(() => {
    env.GITLAB_PROXY_HOST = undefined;
  });
}

export function withTopologyService(url: string) {
  beforeEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = url;
  });
  afterEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = undefined;
  });
}

export function mockTopologyAndBackend(request: Request, cellID: string, classifyType = ClassifyType.CellID) {
  const cellAddress = `cell-${cellID}.example.com`;
  const prefix = classifyType === ClassifyType.SessionPrefix ? 'cell-' : '';

  fetchMock
    .get(env.GITLAB_TOPOLOGY_SERVICE_URL as string)
    .intercept({
      path: CLASSIFY_PATH,
      method: 'POST',
      body: JSON.stringify({ type: classifyType, value: prefix + cellID }),
      headers: { 'Content-Type': 'application/json' },
    })
    .reply(200, { action: 'PROXY', proxy: { address: cellAddress } });

  const url = new URL(request.url);

  fetchMock
    .get(`http://${cellAddress}`)
    .intercept({
      path: request.url,
      headers: { ...{ 'x-forwarded-host': url.host }, ...request.headers },
    })
    .reply(200, `response from ${cellAddress}`);
}

export function mockLegacyResponse(request: Request) {
  const url = new URL(request.url);

  fetchMock
    .get('http://legacy.example.com')
    .intercept({
      path: request.url,
      headers: { ...{ 'x-forwarded-host': url.host }, ...request.headers },
    })
    .reply(200, 'response from legacy');
}

export function mockLegacyErrorResponse(request: Request, error: Error) {
  const url = new URL(request.url);

  fetchMock
    .get('http://legacy.example.com')
    .intercept({
      path: request.url,
      headers: { ...{ 'x-forwarded-host': url.host }, ...request.headers },
    })
    .replyWithError(error);
}

export function generateRoutableToken(payload: string): string {
  const encodedPayload = encodeUrlSafeBase64(`${payload}${generateFakeRandomBytes()}`);
  const payloadLength = encodedPayload.length.toString(36).padStart(2, '0');

  return `${encodedPayload}.${payloadLength}${fakeCRC32Checksum()}`;
}

export async function rulesInfoFetchRequest(rulesInfo: RulesInfo, request: Request): Promise<Response> {
  const ctx = createExecutionContext();
  const logger = buildSilentLogger();
  const rules = buildRulesFromRulesInfo(rulesInfo, logger);
  const engine = new RouterEngine(rules, env);
  const response = await engine.fetch(request, ctx, logger);
  await waitOnExecutionContext(ctx);
  return response;
}

interface RequestOptions {
  cookies?: Record<string, string>;
  headers?: Record<string, string>;
}

function makeHeadersImmutable(headers: Headers): void {
  const throwImmutableHeadersError = () => {
    throw new TypeError("Can't modify immutable headers");
  };

  Object.defineProperties(headers, {
    set: { value: throwImmutableHeadersError },
    append: { value: throwImmutableHeadersError },
    delete: { value: throwImmutableHeadersError },
  });
}

export function buildRequest(url: string | URL, requestOptions: RequestOptions = {}): Request {
  const request = new Request(url);
  const { headers = {}, cookies = {} } = requestOptions;

  // Generic headers
  Object.entries(headers).forEach(([key, value]) => {
    request.headers.set(key, value);
  });

  // Cookies header
  if (Object.keys(cookies).length > 0) {
    const cookieStrings = Object.entries(cookies).map(([key, value]) => `${key}=${value}`);
    request.headers.set('Cookie', cookieStrings.join('; '));
  }

  // To emulate real requests, we need to make
  // sure that requests have immutable headers
  makeHeadersImmutable(request.headers);

  return request;
}

export async function workerFetchURL(url: string): Promise<Response> {
  return workerFetchRequest(buildRequest(url));
}

export async function workerFetchRequest(request: Request): Promise<Response> {
  const ctx = createExecutionContext();
  const response = await worker.fetch(request, env, ctx);
  await waitOnExecutionContext(ctx);
  return response;
}

export async function assertResponseWithRule(request: Request, cellID: number, rule: string) {
  const logger = buildSilentLogger();
  const infoSpy = vi.spyOn(logger, 'info');
  vi.spyOn(Logger, 'getInstance').mockReturnValue(logger);

  mockTopologyAndBackend(request, cellID.toString());

  const response = await workerFetchRequest(request);
  expect(await response.text()).toBe(`response from cell-${cellID}.example.com`);
  expect(infoSpy).toHaveBeenCalledWith(expect.objectContaining({ ruleApplied: rule }));
}

export async function assertProxyResponse(request: Request) {
  mockLegacyResponse(request);

  const response = await workerFetchRequest(request);
  expect(await response.text()).toBe('response from legacy');
}

function generateFakeRandomBytes(): string {
  return 'fake randombytes\x10';
}

function fakeCRC32Checksum(): string {
  return '0123456';
}

export const customMatchers = {
  async toThrowErrorWithMessage<Any>(received: () => Any, errorType: new () => Error, message: string | RegExp) {
    let error: Error;

    try {
      await received();
    } catch (err) {
      error = err as Error;

      if (!(error instanceof errorType)) {
        return {
          pass: false,
          message: () => `Expected to throw ${errorType.name} but it threw ${error.constructor.name} instead.`,
        };
      }

      if (!error.message.match(message)) {
        return {
          pass: false,
          message: () => `Expected to throw ${errorType.name} with message '${message}' but it was '${error.message}'.`,
        };
      }

      return {
        pass: true,
        message: () => `Expected to throw ${errorType.name} and it did.`,
      };
    }

    return {
      pass: false,
      message: () => `Expected to throw ${errorType.name} with message '${message}' but it didn't.`,
    };
  },
};

declare module 'vitest' {
  interface Assertion {
    toThrowErrorWithMessage<Any>(errorConstructor: new (...args: Any[]) => Error, message: string | RegExp): Assertion;
  }
}
