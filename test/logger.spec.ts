import { describe, it, expect, vi } from 'vitest';
import { Logger, buildSilentLogger } from '../src/logger';

describe('Logger', () => {
  describe('.getInstance', () => {
    it('returns the same instance', () => {
      const loggerA: Logger = Logger.getInstance();
      const loggerB: Logger = Logger.getInstance();

      expect(loggerA).toBe(loggerB);
    });
  });

  describe('#log', () => {
    it('it prints all messages when in debug mode', () => {
      const logger: Logger = buildSilentLogger('debug');
      const consoleSpy = vi.spyOn(logger.device, 'log');

      logger.info({ msg: 'Info message' });
      logger.debug({ msg: 'Debug message' });

      expect(consoleSpy).toHaveBeenCalledTimes(2);
      expect(consoleSpy).toHaveBeenCalledWith({ msg: 'Info message' });
      expect(consoleSpy).toHaveBeenCalledWith({ msg: 'Debug message' });
    });

    it('it prints only info messages when in info mode', () => {
      const logger: Logger = buildSilentLogger('info');
      const consoleSpy = vi.spyOn(logger.device, 'log');

      logger.info({ msg: 'Info message' });
      logger.debug({ msg: 'Debug message' });

      expect(consoleSpy).toHaveBeenCalledTimes(1);
      expect(consoleSpy).toHaveBeenCalledWith({ msg: 'Info message' });
    });
  });

  describe('#updateLogLevel', () => {
    it('updates logLevel to debug when passed with debug', () => {
      const logger: Logger = buildSilentLogger('info');

      logger.updateLogLevel('debug');

      expect(logger.logLevel).toEqual('debug');
    });

    it('updates logLevel to info when passed with nonsense', () => {
      const logger: Logger = buildSilentLogger('debug');

      logger.updateLogLevel('nonsense');

      expect(logger.logLevel).toEqual('info');
    });

    it('updates logLevel to info when passed with undefined', () => {
      const logger: Logger = buildSilentLogger('debug');

      logger.updateLogLevel(undefined);

      expect(logger.logLevel).toEqual('info');
    });
  });
});
