import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { classifyFetch } from '../../src/topology_service/classify';
import { TopologyServiceClient } from '../../src/topology_service/client';
import { env, createExecutionContext } from 'cloudflare:test';
import { buildSilentLogger } from '../../src/logger';

describe('classifyFetch', () => {
  const logger = buildSilentLogger();

  beforeEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = 'http://topology-service.example.com';
  });

  afterEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = '';
    vi.restoreAllMocks();
  });

  it('should correctly instantiate client and delegate classify call', async () => {
    const mockClassifyResponse = { success: true };
    const mockClassify = vi.fn().mockResolvedValue(mockClassifyResponse);

    vi.spyOn(TopologyServiceClient.prototype, 'classify').mockImplementation(mockClassify);

    const ctx = createExecutionContext();
    const response = await classifyFetch(env, ctx, logger, 'type', 'value');

    expect(response).toEqual(mockClassifyResponse);
    expect(mockClassify).toHaveBeenCalledWith({ type: 'type', value: 'value' }, ctx);
  });
});
