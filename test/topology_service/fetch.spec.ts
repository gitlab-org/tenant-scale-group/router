import { createExecutionContext, waitOnExecutionContext, fetchMock } from 'cloudflare:test';
import { describe, it, expect, vi } from 'vitest';
import { secureCachedApiFetch, fetchWithRetry } from '../../src/topology_service/fetch';
import { buildSilentLogger, Logger } from '../../src/logger';
import { MaxAttemptsReached, TopologyFetchError } from '../../src/error';
import * as utils from '../../src/utils';
import { RETRYABLE_STATUS_CODES } from '../../src/topology_service/constants';

describe('secureCachedApiFetch', () => {
  const logger = buildSilentLogger();

  it('should fetch from network if not cached and cache the response', async () => {
    fetchMock.get('https://example.com').intercept({ method: 'POST', path: '/', body: 'test' }).reply(200, 'Network Response');

    const cachesDefaultMatchSpy = vi.spyOn(caches.default, 'match');
    const cachesDefaultPutSpy = vi.spyOn(caches.default, 'put');

    const ctx = createExecutionContext();
    const response = await secureCachedApiFetch(ctx, logger, 'https://example.com', { method: 'POST', body: 'test' });
    await waitOnExecutionContext(ctx);

    expect(await response.text()).toBe('Network Response');
    expect(cachesDefaultMatchSpy).toHaveBeenCalledTimes(1);
    expect(cachesDefaultPutSpy).toHaveBeenCalledTimes(1);
  });

  it('should fetch from cache if available', async () => {
    const mockResponse = new Response('cached response');

    const cachesMock = {
      default: {
        match: vi.fn(),
      },
    };
    caches.default.match = cachesMock.default.match;
    const cachesDefaultMatchSpy = vi.spyOn(caches.default, 'match');
    const cachesDefaultPutSpy = vi.spyOn(caches.default, 'put');

    const ctx = createExecutionContext();

    cachesMock.default.match.mockResolvedValue(mockResponse);

    const response = await secureCachedApiFetch(ctx, logger, 'https://example.com', { method: 'POST', body: 'test' });
    await waitOnExecutionContext(ctx);

    expect(response).toBe(mockResponse);
    expect(cachesDefaultMatchSpy).toHaveBeenCalledTimes(1);
    expect(cachesDefaultPutSpy).not.toBeCalled();
  });
});

describe('fetchWithRetry', () => {
  const MAX_ATTEMPTS = 3;

  it('returns the response if the fetch is successful on the first attempt', async () => {
    const mockResponse = new Response('Success', { status: 200 });
    const fetchFn = vi.fn().mockResolvedValue(mockResponse);
    const result = await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);

    expect(fetchFn).toHaveBeenCalledTimes(1);
    expect(result).toBe(mockResponse);
  });

  describe.each(RETRYABLE_STATUS_CODES)('fetchWithRetry on retryable status code %i', (retryableStatusCode) => {
    it(`retries and succeeds after receiving ${retryableStatusCode}`, async () => {
      const mockBadResponse = new Response('Retryable Error', { status: retryableStatusCode });
      const mockSuccessResponse = new Response('Success', { status: 200 });
      const fetchFn = vi.fn().mockResolvedValueOnce(mockBadResponse).mockResolvedValueOnce(mockSuccessResponse);
      const result = await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);

      expect(fetchFn).toHaveBeenCalledTimes(2);
      expect(result).toBe(mockSuccessResponse);
    });
  });

  it('retries on TopologyFetchError', async () => {
    const mockTopologyFetchError = new TopologyFetchError();
    const mockSuccessResponse = new Response('Success', { status: 200 });
    const fetchFn = vi.fn().mockRejectedValueOnce(mockTopologyFetchError).mockResolvedValueOnce(mockSuccessResponse);
    const result = await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);

    expect(fetchFn).toHaveBeenCalledTimes(2);
    expect(result).toBe(mockSuccessResponse);
  });

  it('does not retry on non-retryable status codes (e.g., 400, 404, 429)', async () => {
    const mockFailedResponse = new Response('Not Found', { status: 404 });
    const fetchFn = vi.fn().mockResolvedValue(mockFailedResponse);
    const result = await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);

    expect(fetchFn).toHaveBeenCalledTimes(1);
    expect(result).toBe(mockFailedResponse);
  });

  it('stops retrying and throws an error after reaching the maximum number of attempts', async () => {
    const mockResponse = new Response('Retryable Error', { status: 503 });
    const fetchFn = vi.fn().mockResolvedValue(mockResponse);

    await expect(async () => {
      await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);
    }).toThrowErrorWithMessage(MaxAttemptsReached, `Still failing after ${MAX_ATTEMPTS} attempts.`);

    expect(fetchFn).toHaveBeenCalledTimes(MAX_ATTEMPTS);
  });

  it('stops retrying and throws an error after reaching the maximum number of attempts for TopologyFetchError', async () => {
    const logger = Logger.getInstance();
    const loggerSpy = vi.spyOn(logger, 'info');

    const mockTopologyFetchError = new TopologyFetchError('The operation timed out.');
    const fetchFn = vi.fn().mockRejectedValue(mockTopologyFetchError);

    await expect(async () => {
      await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);
    }).toThrowErrorWithMessage(MaxAttemptsReached, `Still failing after ${MAX_ATTEMPTS} attempts.`);

    expect(fetchFn).toHaveBeenCalledTimes(MAX_ATTEMPTS);
    expect(loggerSpy).toHaveBeenCalledTimes(4);
    expect(loggerSpy).toHaveBeenCalledWith(
      expect.objectContaining({
        nextDelay: 0,
        attempt: 1,
      }),
    );
    expect(loggerSpy).toHaveBeenCalledWith(
      expect.objectContaining({
        nextDelay: 0,
        attempt: 2,
      }),
    );
    expect(loggerSpy).toHaveBeenCalledWith(
      expect.objectContaining({
        nextDelay: undefined,
        attempt: 3,
      }),
    );
    expect(loggerSpy).toHaveBeenCalledWith(
      expect.objectContaining({
        maxAttempts: MAX_ATTEMPTS,
      }),
    );
  });

  it('applies exponential backoff between retries', async () => {
    const mockResponse = new Response('Retryable Error', { status: 504 });
    const mockSuccessResponse = new Response('Success', { status: 200 });

    const fetchFn = vi
      .fn()
      .mockResolvedValueOnce(mockResponse)
      .mockResolvedValueOnce(mockResponse)
      .mockResolvedValueOnce(mockResponse)
      .mockResolvedValueOnce(mockResponse)
      .mockResolvedValueOnce(mockSuccessResponse);

    const sleepPromiseSpy = vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());
    vi.spyOn(Math, 'random').mockReturnValue(0.7);
    const result = await fetchWithRetry(fetchFn, 10, 2, 5);

    expect(fetchFn).toHaveBeenCalledTimes(5);
    expect(sleepPromiseSpy).toHaveBeenCalledTimes(4);
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(1, expect.closeTo(7));
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(2, expect.closeTo(14));
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(3, expect.closeTo(28));
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(4, expect.closeTo(56));

    expect(result).toBe(mockSuccessResponse);
  });

  it('does not sleep on the last retry', async () => {
    const mockResponse = new Response('Retryable Error', { status: 504 });

    const fetchFn = vi.fn().mockResolvedValue(mockResponse);

    const sleepPromiseSpy = vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());
    vi.spyOn(Math, 'random').mockReturnValue(1);

    await expect(async () => {
      await fetchWithRetry(fetchFn, 10, 2, MAX_ATTEMPTS);
    }).toThrowErrorWithMessage(MaxAttemptsReached, `Still failing after ${MAX_ATTEMPTS} attempts.`);

    expect(fetchFn).toHaveBeenCalledTimes(MAX_ATTEMPTS);
    expect(sleepPromiseSpy).toHaveBeenCalledTimes(MAX_ATTEMPTS - 1);
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(1, expect.closeTo(10));
    expect(sleepPromiseSpy).toHaveBeenNthCalledWith(2, expect.closeTo(20));
  });

  it('throws an error if a non-retryable error occurs', async () => {
    const fetchFn = vi.fn().mockRejectedValueOnce(new Error('Network error'));

    await expect(async () => {
      await fetchWithRetry(fetchFn, 0, 0, MAX_ATTEMPTS);
    }).toThrowErrorWithMessage(Error, 'Network error');

    expect(fetchFn).toHaveBeenCalledTimes(1);
  });
});
