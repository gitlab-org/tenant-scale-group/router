import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { TopologyServiceClient } from '../../src/topology_service/client';
import * as fetch from '../../src/topology_service/fetch';
import { env, createExecutionContext } from 'cloudflare:test';
import { buildSilentLogger } from '../../src/logger';
import { MaxAttemptsReached, NotFound, TopologyFetchError } from '../../src/error';
import * as utils from '../../src/utils';
import { CLASSIFY_USER_AGENT } from '../../src/topology_service/constants';

describe('TopologyServiceClient', () => {
  let secureCachedApiFetchSpy: ReturnType<typeof vi.spyOn>;
  let abortTimeoutSpy: ReturnType<typeof vi.spyOn>;
  const logger = buildSilentLogger();

  beforeEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = 'http://topology-service.example.com';
    secureCachedApiFetchSpy = vi.spyOn(fetch, 'secureCachedApiFetch') as ReturnType<typeof vi.spyOn>;
    abortTimeoutSpy = vi.spyOn(AbortSignal, 'timeout') as ReturnType<typeof vi.spyOn>;
  });

  afterEach(() => {
    env.GITLAB_TOPOLOGY_SERVICE_URL = '';
    env.GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS = undefined;
    vi.restoreAllMocks();
  });

  describe('topologyServiceTimeoutMs', () => {
    describe('when GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS is not specified', () => {
      it('uses default timeout value', async () => {
        const client = new TopologyServiceClient(env, logger);
        const mockResponse = { ok: true, json: vi.fn().mockResolvedValue({}) };
        secureCachedApiFetchSpy.mockResolvedValue(mockResponse);

        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());

        expect(abortTimeoutSpy).toHaveBeenCalledWith(1000);
      });
    });

    describe('when GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS is 0', () => {
      beforeEach(() => {
        env.GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS = 0;
      });

      it('uses default timeout value', async () => {
        const client = new TopologyServiceClient(env, logger);
        const mockResponse = { ok: true, json: vi.fn().mockResolvedValue({}) };
        secureCachedApiFetchSpy.mockResolvedValue(mockResponse);

        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());

        expect(abortTimeoutSpy).toHaveBeenCalledWith(1000);
      });
    });

    describe('when GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS is negative', () => {
      beforeEach(() => {
        env.GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS = -200;
      });

      it('uses default timeout value', async () => {
        const client = new TopologyServiceClient(env, logger);
        const mockResponse = { ok: true, json: vi.fn().mockResolvedValue({}) };
        secureCachedApiFetchSpy.mockResolvedValue(mockResponse);

        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());

        expect(abortTimeoutSpy).toHaveBeenCalledWith(1000);
      });
    });

    describe('when GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS is positive', () => {
      beforeEach(() => {
        env.GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS = 400;
      });

      it('uses specified timeout value', async () => {
        const client = new TopologyServiceClient(env, logger);
        const mockResponse = { ok: true, json: vi.fn().mockResolvedValue({}) };
        secureCachedApiFetchSpy.mockResolvedValue(mockResponse);

        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());

        expect(abortTimeoutSpy).toHaveBeenCalledWith(400);
      });
    });
  });

  describe('classify', () => {
    it('should send correct request and handle successful response', async () => {
      const mockResponse = {
        ok: true,
        json: vi.fn().mockResolvedValue({ success: true }),
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const ctx = createExecutionContext();
      const client = new TopologyServiceClient(env, logger);

      const response = await client.classify({ type: 'type', value: 'value' }, ctx);

      expect(response).toEqual({ success: true });
      expect(secureCachedApiFetchSpy).toHaveBeenCalledWith(ctx, logger, `http://topology-service.example.com/v1/classify`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': CLASSIFY_USER_AGENT,
        },
        body: JSON.stringify({ type: 'type', value: 'value' }),
        signal: expect.any(AbortSignal),
      });
    });

    it('should throw a NotFound error when Topology Service gives 404 error', async () => {
      const mockResponse = {
        ok: false,
        status: 404,
        statusText: 'Not Found',
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const client = new TopologyServiceClient(env, logger);

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());
      }).toThrowErrorWithMessage(NotFound, 'Client Error: Not Found');
    });

    it('should throw MaxAttemptsReached when Topology Service gives an error', async () => {
      secureCachedApiFetchSpy.mockRejectedValue(new TopologyFetchError('The operation timed out.'));

      const client = new TopologyServiceClient(env, logger);
      const ctx = createExecutionContext();

      vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, ctx);
      }).toThrowErrorWithMessage(MaxAttemptsReached, 'Still failing after 3 attempts.');

      expect(secureCachedApiFetchSpy).toHaveBeenCalledWith(ctx, logger, `http://topology-service.example.com/v1/classify`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': CLASSIFY_USER_AGENT,
        },
        body: JSON.stringify({ type: 'type', value: 'value' }),
        signal: expect.any(AbortSignal),
      });
    });

    it('should handle client errors (4xx)', async () => {
      const mockResponse = {
        ok: false,
        status: 400,
        statusText: 'Bad Request',
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const client = new TopologyServiceClient(env, logger);

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());
      }).toThrowErrorWithMessage(Error, 'Client Error: Bad Request');
    });

    it('should handle server errors with retryable status codes', async () => {
      const mockResponse = {
        ok: false,
        status: 500,
        statusText: 'Internal Server Error',
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const client = new TopologyServiceClient(env, logger);

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());
      }).toThrowErrorWithMessage(MaxAttemptsReached, 'Still failing after 3 attempts.');
    });

    it('should handle server errors with non-retryable status codes', async () => {
      const mockResponse = {
        ok: false,
        status: 501,
        statusText: 'Not Implemented',
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const client = new TopologyServiceClient(env, logger);

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());
      }).toThrowErrorWithMessage(Error, 'Topology Service is not reachable.');
    });

    it('should handle other non-OK responses', async () => {
      const mockResponse = {
        ok: false,
        status: 300,
        statusText: 'Multiple Choices',
      };
      secureCachedApiFetchSpy.mockResolvedValue(mockResponse);
      const client = new TopologyServiceClient(env, logger);

      await expect(async () => {
        await client.classify({ type: 'type', value: 'value' }, createExecutionContext());
      }).toThrowErrorWithMessage(Error, 'Service is not reachable. Multiple Choices');
    });
  });
});
