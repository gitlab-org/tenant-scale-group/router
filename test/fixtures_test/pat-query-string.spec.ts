import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest, generateRoutableToken } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';

import patQueryString from '../fixtures/pat-query-string.json';

describe('pat-query-string.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (123).toString(36);
  const queryString = generateRoutableToken(`c:${encodedCellID}`);

  it('classifies using the decoded payload', async () => {
    const request = new Request(`http://example.com?private_token=glpat-${queryString}`);

    mockTopologyAndBackend(request, '123');

    const response = await rulesInfoFetchRequest(patQueryString as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-123.example.com');
  });
});
