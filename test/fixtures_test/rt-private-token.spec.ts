import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest, generateRoutableToken } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';

import rtPrivateToken from '../fixtures/rt-private-token.json';

describe('rt-private-token.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (42).toString(36);
  const privateTokenHeader = `glrt-${generateRoutableToken(`r:bytes\nc:${encodedCellID}`)}`;

  it('classifies using the decoded payload', async () => {
    const request = new Request('http://example.com/', { headers: { 'Private-Token': privateTokenHeader } });

    mockTopologyAndBackend(request, '42');

    const response = await rulesInfoFetchRequest(rtPrivateToken as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-42.example.com');
  });
});
