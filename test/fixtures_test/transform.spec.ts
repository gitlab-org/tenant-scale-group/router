import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest, generateRoutableToken } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';
import { encodeUrlSafeBase64 } from '../../src/utils';

import transform from '../fixtures/transform.json';

describe('transform.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (135).toString(36);

  it('classifies `routable-token` using the the decoded payload', async () => {
    const request = new Request(`http://example.com/-/test-routable-token/${generateRoutableToken(`c:${encodedCellID}`)}`);

    mockTopologyAndBackend(request, '135');

    const response = await rulesInfoFetchRequest(transform as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-135.example.com');
  });

  it('classifies `routable-jwt` using the the decoded payload prioritizing the `c` key', async () => {
    const payload = { c: encodedCellID, o: '1', p: '2', u: '3', g: '4' };
    const request = new Request(
      `http://example.com/-/test-routable-jwt/${btoa('header')}.${encodeUrlSafeBase64(JSON.stringify(payload))}.${btoa('signature')}`,
    );

    mockTopologyAndBackend(request, '135');

    const response = await rulesInfoFetchRequest(transform as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-135.example.com');
  });
});
