import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';
import { encodeUrlSafeBase64 } from '../../src/utils';

import jwtTokenQueryString from '../fixtures/jwt-token-query-string.json';

describe('jwt-token-query-string.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (420).toString(36);
  const payload = { c: encodedCellID, o: '1', p: '2', u: '3', g: '4' };
  const jobToken = `${btoa('header')}.${encodeUrlSafeBase64(JSON.stringify(payload))}.${btoa('signature')}`;

  it('classifies using the decoded payload', async () => {
    const request = new Request(`http://example.com?job_token=${jobToken}`);

    mockTopologyAndBackend(request, '420');

    const response = await rulesInfoFetchRequest(jwtTokenQueryString as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-420.example.com');
  });
});
