import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest, generateRoutableToken } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';

import patAuthorization from '../fixtures/pat-authorization.json';

describe('pat-authorization.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (777).toString(36);
  const authorizationHeader = `Bearer glpat-${generateRoutableToken(`c:${encodedCellID}`)}`;

  it('classifies using the decoded payload', async () => {
    const request = new Request('http://example.com/', {
      headers: { Authorization: authorizationHeader },
    });

    mockTopologyAndBackend(request, '777');

    const response = await rulesInfoFetchRequest(patAuthorization as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-777.example.com');
  });
});
