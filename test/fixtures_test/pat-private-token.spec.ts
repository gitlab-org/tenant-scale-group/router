import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest, generateRoutableToken } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';

import patPrivateToken from '../fixtures/pat-private-token.json';

describe('pat-private-token.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (1024).toString(36);
  const privateTokenHeader = `glpat-${generateRoutableToken(`c:${encodedCellID}`)}`;

  it('classifies using the decoded payload', async () => {
    const request = new Request('http://example.com/', { headers: { 'Private-Token': privateTokenHeader } });

    mockTopologyAndBackend(request, '1024');

    const response = await rulesInfoFetchRequest(patPrivateToken as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-1024.example.com');
  });
});
