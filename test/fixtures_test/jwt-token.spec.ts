import { withTopologyService, mockTopologyAndBackend, rulesInfoFetchRequest } from '../helper';
import { describe, it, expect } from 'vitest';
import { RulesInfo } from '../../src/rules/types.d';
import { encodeUrlSafeBase64 } from '../../src/utils';

import jwtTokenHeader from '../fixtures/jwt-token-header.json';

describe('jwt-token-header.json', () => {
  withTopologyService('http://topology-service.example.com');
  const encodedCellID = (777).toString(36);
  const payload = { c: encodedCellID, o: '1', p: '2', u: '3', g: '4' };

  it('classifies using the decoded payload', async () => {
    const request = new Request('http://example.com/', {
      headers: { 'JOB-TOKEN': `${btoa('header')}.${encodeUrlSafeBase64(JSON.stringify(payload))}.${btoa('signature')}` },
    });

    mockTopologyAndBackend(request, '777');

    const response = await rulesInfoFetchRequest(jwtTokenHeader as RulesInfo, request);
    expect(await response.text()).toBe('response from cell-777.example.com');
  });
});
