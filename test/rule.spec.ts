import { describe, it, expect } from 'vitest';
import { VersionRule } from '../src/rules/version';
import { InvalidTokenPayloadError, InvalidUrlSafeBase64 } from '../src/error';
import { buildSilentLogger } from '../src/logger';
import {
  VersionRuleInfo,
  ActionType,
  MatchResult,
  ValidateInfo,
  MaybeMatchResult,
  MatchInfo,
  TransformInfo,
  MatchType,
} from '../src/rules/types.d';
import { encodeUrlSafeBase64 } from '../src/utils';
import { generateRoutableToken, buildRequest } from './helper';

class FakeRule extends VersionRule {
  public callInterpolateValue(matchResult: MatchResult, value: string): string {
    return this.interpolateValue(matchResult, value);
  }

  public callTransformMatchResult(matchResult: MatchResult): MatchResult {
    // as any is a trick to call a private method
    /* eslint-disable @typescript-eslint/no-explicit-any */
    return (this as any).transformMatchResult(matchResult);
    /* eslint-enable @typescript-eslint/no-explicit-any */
  }

  public callValidateMatchResult(matchResult: MatchResult): MaybeMatchResult {
    // as any is a trick to call a private method
    /* eslint-disable @typescript-eslint/no-explicit-any */
    return (this as any).isMatchResultValid(matchResult);
    /* eslint-enable @typescript-eslint/no-explicit-any */
  }
}

function newFakeRule(matchInfo?: MatchInfo, transformInfo?: TransformInfo, validateInfo?: ValidateInfo): FakeRule {
  const ruleInfo: VersionRuleInfo = {
    match: matchInfo,
    transform: transformInfo,
    validate: validateInfo,
    id: 'id',
    action: ActionType.Version,
  };

  return new FakeRule(ruleInfo, buildSilentLogger());
}

describe('Rule', () => {
  describe('#match', () => {
    it('returns an empty object with missing configuration', async () => {
      const rule = newFakeRule();
      const request = buildRequest('http://example.com');
      const result = rule.match(request);

      expect(result).toStrictEqual({});
    });

    describe('with valid configuration', () => {
      const rule = newFakeRule(
        { type: MatchType.Path, regex_name: 'path', regex_value: '^/-/test-routable-token/(?<payload>[0-9A-Za-z_-]+)' },
        { type: 'routable-token-payload', input: ['${payload}'], output: 'decoded' },
        { exist: ['${decoded.c}'] },
      );

      it('returns null if there is no match', async () => {
        const request = buildRequest('http://example.com');
        const result = rule.match(request);

        expect(result).toBeNull();
      });

      it('should return null when required validation keys are missing', async () => {
        const encodedCellID = (135).toString(36);
        const request = buildRequest(`http://example.com/-/test-routable-token/${generateRoutableToken(`d:${encodedCellID}`)}`);
        const result = rule.match(request);

        expect(result).toBeNull();
      });

      it('should return match result when validation passes', async () => {
        const encodedCellID = (135).toString(36);
        const request = buildRequest(`http://example.com/-/test-routable-token/${generateRoutableToken(`c:${encodedCellID}`)}`);
        const result = rule.match(request);

        expect(result).toBeTruthy();
        expect(result).toHaveProperty('decoded.c');
      });
    });
  });

  describe('#interpolateValue', () => {
    const rule = new FakeRule({} as VersionRuleInfo, buildSilentLogger());

    it('interpolates cell-${cell_name}', async () => {
      const result = rule.callInterpolateValue({ cell_name: '123' }, 'cell-${cell_name}');

      expect(result).toBe('cell-123');
    });

    it('interpolates cell-${decoded.c}', async () => {
      const result = rule.callInterpolateValue({ 'decoded.c': '123' }, 'cell-${decoded.c}');

      expect(result).toBe('cell-123');
    });

    it('does not interpolate cell-${@}', async () => {
      const result = rule.callInterpolateValue({ '@': '123' }, 'cell-${@}');

      expect(result).toBe('cell-${@}');
    });
  });

  describe('#transformMatchResult', () => {
    const matchInfo: MatchInfo = {
      type: MatchType.Path,
      regex_name: 'path',
      regex_value: '^/(?<payload>[0-9A-Za-z_-]+)\\.(?<payload_length>[0-9a-z]{2})$',
    };

    describe('when type is routable-token-payload', () => {
      const rule = newFakeRule(matchInfo, {
        type: 'routable-token-payload',
        input: ['${payload}', '${payload_length}'],
        output: 'decoded',
      });

      function generateMatchResult(payload: string): MatchResult {
        // The null character is the size of random bytes appended.
        // We just put it null so it means there's no random bytes.
        const encodedPayload = encodeUrlSafeBase64(`${payload}\0`);
        return { payload: encodedPayload, payload_length: encodedPayload.length.toString(36) };
      }

      it('transforms the match results and accept only valid routing keys', async () => {
        const encodedBigInt = BigInt(2n ** 63n).toString(36);
        const matchResult = generateMatchResult(`a:6\nc:1\nnon-existent:7\ng:2\no:3\np:4\nu:${encodedBigInt}`);
        const result = rule.callTransformMatchResult(matchResult);

        expect(result).toStrictEqual({
          ...matchResult,
          ...{ 'decoded.c': '1', 'decoded.g': '2', 'decoded.o': '3', 'decoded.p': '4', 'decoded.u': '9223372036854775808' },
        });
      });

      it('throws InvalidTokenPayloadError if payload contains a key or value surrounded by whitespace', async () => {
        const matchResult = generateMatchResult('c: 1 ');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid base36 value:  1 ');
      });

      it('throws InvalidTokenPayloadError if payload ends with the newline character', async () => {
        const matchResult = generateMatchResult('c:1\n');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: missing separator ":" in ');
      });

      it('throws InvalidUrlSafeBase64 if payload is not base64 encoded', async () => {
        const matchResult = { payload: 'c:1\nu:2', payload_length: '7' };

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidUrlSafeBase64, 'Invalid URL safe base64:');
      });

      it('throws InvalidTokenPayloadError if payload has an empty value', async () => {
        const matchResult = generateMatchResult('c:');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: key or value missing in c:');
      });

      it('throws InvalidTokenPayloadError if payload has an empty key', async () => {
        const matchResult = generateMatchResult(':c');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: key or value missing in :c');
      });

      it('throws InvalidTokenPayloadError if payload has bad format', async () => {
        const matchResult = generateMatchResult('invalid');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: missing separator ":" in invalid');
      });

      it('throws InvalidTokenPayloadError if value is not base36 encoded', async () => {
        const matchResult = generateMatchResult('c:36>');

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid base36 value: 36>');
      });
    });

    describe('when type is routable-json', () => {
      const rule = newFakeRule(matchInfo, { type: 'routable-json', input: ['${payload}', '${payload_length}'], output: 'decoded' });

      it('throws InvalidUrlSafeBase64 if payload is not base64 encoded', async () => {
        const matchResult = { payload: JSON.stringify({ c: 1, u: 2 }) };

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidUrlSafeBase64, 'Invalid URL safe base64:');
      });

      it('throws InvalidTokenPayloadError if payload has an empty value', async () => {
        const matchResult = { payload: encodeUrlSafeBase64(JSON.stringify({ c: '' })) };

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: key or value missing in c:');
      });

      it('throws InvalidTokenPayloadError if payload has an empty key', async () => {
        const matchResult = { payload: encodeUrlSafeBase64(JSON.stringify({ '': 'c' })) };

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(InvalidTokenPayloadError, 'Invalid pair: key or value missing in :c');
      });

      it('throws SyntaxError if payload has bad format', async () => {
        const matchResult = { payload: encodeUrlSafeBase64('invalid') };

        await expect(() => {
          rule.callTransformMatchResult(matchResult);
        }).toThrowErrorWithMessage(SyntaxError, `Unexpected token 'i', "invalid" is not valid JSON`);
      });
    });
  });

  describe('#isMatchResultValid', () => {
    const matchInfo: MatchInfo = { type: MatchType.Path, regex_name: 'path', regex_value: '^/(?<payload>[0-9A-Za-z_-]+)$' };
    const transformInfo: TransformInfo = { type: 'routable-token-payload', input: ['${payload}'], output: 'decoded' };

    describe('when validate is configured', () => {
      const rule = newFakeRule(matchInfo, transformInfo, { exist: ['${decoded.c}'] });

      it('returns true when all required keys exists', () => {
        const matchResult = { 'decoded.c': '1', 'decoded.g': '2' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(true);
      });

      it('returns false when required keys is missing', () => {
        const matchResult = { 'decoded.g': '2' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(false);
      });
    });

    describe('when validate is not configured', () => {
      const rule = newFakeRule();

      it('returns true', () => {
        const matchResult = { 'decoded.c': '2' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(true);
      });
    });

    describe('when validate has multiple keys', () => {
      const rule = newFakeRule(matchInfo, transformInfo, { exist: ['${decoded.c}', '${decoded.g}'] });

      it('returns true when all keys exist', () => {
        const matchResult = { 'decoded.c': '1', 'decoded.g': '2', 'decoded.e': '3' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(true);
      });

      it('returns false when any required key is missing', () => {
        const matchResult = { 'decoded.c': '1', 'decoded.e': '3' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(false);
      });
    });

    describe('when validate has an invalid value defined', () => {
      const rule = newFakeRule(matchInfo, transformInfo, { exist: ['${invalid}'] });

      it('returns false', () => {
        const matchResult = { 'decoded.c': '1' };

        const result = rule.callValidateMatchResult(matchResult);
        expect(result).toBe(false);
      });
    });
  });
});
