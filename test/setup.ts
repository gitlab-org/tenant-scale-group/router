import { fetchMock } from 'cloudflare:test';
import { beforeAll, beforeEach, afterEach, vi, expect } from 'vitest';
import { customMatchers } from './helper';
import { Logger, buildSilentLogger } from '../src/logger';

expect.extend(customMatchers);

beforeAll(() => {
  // Enable outbound request mocking...
  fetchMock.activate();
  // ...and throw errors if an outbound request isn't mocked
  fetchMock.disableNetConnect();
});

beforeEach(() => {
  // We should never actually log anything in the tests
  vi.spyOn(Logger, 'getInstance').mockReturnValue(buildSilentLogger());
});

// Ensure we matched every mock we defined
afterEach(() => {
  fetchMock.assertNoPendingInterceptors();
  vi.restoreAllMocks();
});
