import { describe, it, expect } from 'vitest';
import { base36ToBigInt, decodeUrlSafeBase64, encodeUrlSafeBase64, sha256 } from '../src/utils';
import { InvalidUrlSafeBase64 } from '../src/error';

describe('URL-safe base64 utilities', () => {
  const bytes = '\x86\x14\xC2\xDBh\x16Pp\xD7\xB3\xEF\xCA\x8F\x96\xD8\xDF';
  const encoded = 'hhTC22gWUHDXs-_Kj5bY3w';

  describe('encodeUrlSafeBase64', () => {
    it('encodes with URL safe base64', async () => {
      expect(encodeUrlSafeBase64(bytes)).toBe(encoded);
    });
  });

  describe('decodeUrlSafeBase64', () => {
    it('decodes with URL safe base64', async () => {
      expect(decodeUrlSafeBase64(encoded)).toBe(bytes);
    });

    it('raises InvalidUrlSafeBase64 error for not correctly encoded string', async () => {
      await expect(() => {
        decodeUrlSafeBase64('gitl@b');
      }).toThrowErrorWithMessage(InvalidUrlSafeBase64, 'Invalid URL safe base64:');
    });
  });
});

describe('base36ToBigInt', () => {
  it('converts a valid base36 string to BigInt', () => {
    expect(base36ToBigInt('1')).toBe(1n);
    expect(base36ToBigInt('10')).toBe(36n);
    expect(base36ToBigInt('z')).toBe(35n);
    expect(base36ToBigInt('1y2p0ij32e0og')).toBe(9223372036854765808n);
  });

  it('handles large base36 numbers correctly', () => {
    const largeBase36 = BigInt(2n ** 63n).toString(36); //1y2p0ij32e8e8
    const bigInt = BigInt(9223372036854775808n);

    expect(base36ToBigInt(largeBase36)).toBe(bigInt);
  });

  it('returns 0n for an empty string', () => {
    expect(base36ToBigInt('')).toBe(0n);
  });

  it('throws RangeError for invalid base36 characters', async () => {
    await expect(() => {
      base36ToBigInt('gitl@b');
    }).toThrowErrorWithMessage(RangeError, /The number NaN cannot be converted to a BigInt because it is not an integer/i);

    await expect(() => {
      base36ToBigInt('123!');
    }).toThrowErrorWithMessage(RangeError, /The number NaN cannot be converted to a BigInt because it is not an integer/i);
  });
});

describe('sha256', () => {
  it('returns the correct SHA-256 hash for a given string', async () => {
    const input = 'gitlab';
    const expectedHash = '9d96d9d5b1addd7e7e6119a23b1e5b5f68545312bfecb21d1cdc6af22b8628b8';
    const result = await sha256(input);
    expect(result).toBe(expectedHash);
  });

  it('handles an empty string correctly', async () => {
    const input = '';
    const expectedHash = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';
    const result = await sha256(input);
    expect(result).toBe(expectedHash);
  });

  it('handles special characters correctly', async () => {
    const input = `!@#$%^&*()_+-={}[]|\\:";'<> ?,./`;
    const expectedHash = 'd03c2e7f94ace1ae27cefef65683edfa2833a9cd30544a209f2fcfd43bea45ea';
    const result = await sha256(input);
    expect(result).toBe(expectedHash);
  });

  it('handles Unicode characters correctly', async () => {
    const input = '你好世界';
    const expectedHash = 'beca6335b20ff57ccc47403ef4d9e0b8fccb4442b3151c2e7d50050673d43172';
    const result = await sha256(input);
    expect(result).toBe(expectedHash);
  });
});
