import { env } from 'cloudflare:test';
import { describe, it, expect, vi } from 'vitest';
import { withRulesConfig, workerFetchURL } from './helper';

describe('when rules are not configured', () => {
  withRulesConfig('');

  it('returns a Promise.reject', async () => {
    const spy = vi.fn((error) => {
      expect(error.message).toBe('The GITLAB_RULES_CONFIG is not configured.');
    });
    const promise = workerFetchURL('http://example.com');
    await promise.catch(spy);

    expect(spy).toHaveBeenCalled();
  });
});

describe('when rules are configured', () => {
  describe('with invalid rules configuration', () => {
    withRulesConfig('invalid');

    it('returns a Promise.reject', async () => {
      const spy = vi.fn((error) => {
        expect(error.message).toBe(`The '${env.GITLAB_RULES_CONFIG}' was not found.`);
      });
      const promise = workerFetchURL('http://example.com');
      await promise.catch(spy);

      expect(spy).toHaveBeenCalled();
    });
  });
});
