import { describe, it, expect } from 'vitest';
import { HeaderMatchInfo, MatchType, RegexMatchInfo } from '../../src/rules/types.d';
import { buildRequest } from '../helper';
import {
  getCookieValue,
  getQueryValue,
  matchCookie,
  matchRequest,
  matchPath,
  matchHeader,
  matchQueryString,
} from '../../src/rules/match_request';

function buildRequestWithHeader(name: string, value: string): Request {
  return buildRequest('http://example.com/test/path', { headers: { [name]: value } });
}

function buildRequestWithCookie(value: string): Request {
  return buildRequestWithHeader('Cookie', value);
}

function buildURLWithSearchParams(params: Record<string, string | number | boolean>): URL {
  const url = new URL('http://example.com/test/path');

  Object.entries(params).forEach(([key, value]) => {
    url.searchParams.append(key, value.toString());
  });
  return url;
}

describe('getCookieValue', () => {
  const name = '_gitlab_session';

  describe('with single cookie value with dashes', () => {
    it('returns the value', async () => {
      const expected = 'cells-1-fake-session-id';
      const value = getCookieValue(name, buildRequestWithCookie(`${name}=${expected}`));

      expect(value).toBe(expected);
    });
  });

  describe('with multiple cookies value with underscores', () => {
    it('returns the values we are looking for', async () => {
      const expected = 'cells-1-fake_session_id';
      const value = getCookieValue(name, buildRequestWithCookie(`first=one; ${name}=${expected}`));

      expect(value).toBe(expected);
    });
  });

  describe('with multiple cookies value and target at first', () => {
    it('returns the values we are looking for', async () => {
      const expected = 'cells-1-fake-session-id';
      const value = getCookieValue(name, buildRequestWithCookie(`${name}=${expected}; last=two`));

      expect(value).toBe(expected);
    });
  });

  describe('with multiple cookies with similar starting names', () => {
    it('returns the values we are looking for', async () => {
      const expected = 'cells-1-fake-session-id';
      const value = getCookieValue(name, buildRequestWithCookie(`${name}-bad=bad; ${name}_bad=bad; ${name}=${expected}`));

      expect(value).toBe(expected);
    });
  });

  describe('with multiple cookies with similar ending names', () => {
    it('returns the values we are looking for', async () => {
      const expected = 'cells-1-fake-session-id';
      const value = getCookieValue(name, buildRequestWithCookie(`bad-${name}=bad; bad_${name}=bad; ${name}=${expected}`));

      expect(value).toBe(expected);
    });
  });

  describe('when there is no matching name', () => {
    it('returns null', async () => {
      const expected = null;
      const value = getCookieValue(name, buildRequestWithCookie(`${name}_=${expected}`));

      expect(value).toBe(expected);
    });
  });
});

describe('getQueryValue', () => {
  it('should return the value of an existing query parameter', () => {
    const url = new URL('https://example.com?git=Lab&foo=bar');
    expect(getQueryValue('git', url)).toBe('Lab');
  });

  it('should return the value of a different query parameter', () => {
    const url = new URL('https://example.com?git=Lab&foo=bar');
    expect(getQueryValue('foo', url)).toBe('bar');
  });

  it('should return null if the query parameter does not exist', () => {
    const url = new URL('https://example.com?git=Lab&foo=bar');
    expect(getQueryValue('cell-id', url)).toBeNull();
  });

  it('should return an empty string if the query parameter exists but has no value', () => {
    const url = new URL('https://example.com?git=&foo=bar');
    expect(getQueryValue('git', url)).toBe('');
  });

  it('should handle multiple query parameters with the same name and return the first one', () => {
    const url = new URL('https://example.com?git=Lab&git=Hub&foo=bar');
    expect(getQueryValue('git', url)).toBe('Lab');
  });

  it('should return null for URLs without query parameters', () => {
    const url = new URL('https://example.com');
    expect(getQueryValue('git', url)).toBeNull();
  });

  it('should be case-insensitive when searching for query parameters', () => {
    const url = new URL('https://example.com?Git=Lab&Foo=Bar');
    expect(getQueryValue('git', url)).toBe('Lab');
  });

  it('should not match substrings', () => {
    const url = new URL('https://example.com?gitlab=com');
    expect(getQueryValue('git', url)).toBeNull();
  });

  it('should match using a regular expression pattern', () => {
    const url = new URL('https://example.com?gitlab=value');

    expect(getQueryValue('git...', url)).toBe('value');
  });
});

describe('matchCookie', () => {
  const name = '_gitlab_session';

  function buildMatchInfo(regex_value: string): RegexMatchInfo {
    return { type: MatchType.Cookie, regex_name: name, regex_value: regex_value };
  }

  it('returns the capture groups', async () => {
    const expected = { cell_name: 'cell-4' };
    const result = matchCookie(buildMatchInfo('^(?<cell_name>cell-\\d+)-.*'), buildRequestWithCookie(`${name}=cell-4-fake_session_id`));

    expect(result).toEqual(expected);
  });

  describe('when it does not match', () => {
    it('returns null', async () => {
      const expected = null;
      const result = matchCookie(buildMatchInfo('^(?<cell_name>cell-\\d+)-.*'), buildRequestWithCookie(`${name}=bell-4-fake_session_id`));

      expect(result).toBe(expected);
    });
  });

  describe('when it matches but there is no capture group', () => {
    it('returns {}', async () => {
      const expected = {};
      const result = matchCookie(buildMatchInfo('^cell-\\d+-.*'), buildRequestWithCookie(`${name}=cell-4-fake_session_id`));

      expect(result).toEqual(expected);
    });
  });
});

describe('matchHeader', () => {
  const name = 'FoRcE-CelL';
  function buildMatchInfo(value: string): HeaderMatchInfo {
    return { type: MatchType.Header, name: name, regex_value: value };
  }

  describe('when it matches with capture group', () => {
    it('returns the capture groups', async () => {
      const expected = { cell_name: 'cell-4' };
      const result = matchHeader(buildMatchInfo('^(?<cell_name>cell-\\d+)$'), buildRequestWithHeader(name, `cell-4`));

      expect(result).toEqual(expected);
    });

    it('returns the capture groups for lower case key', async () => {
      const expected = { cell_name: 'cell-4' };
      const result = matchHeader(buildMatchInfo('^(?<cell_name>cell-\\d+)$'), buildRequestWithHeader(name.toLocaleLowerCase(), 'cell-4'));

      expect(result).toEqual(expected);
    });
  });

  describe('when it does not match', () => {
    it('returns null', async () => {
      const expected = null;
      const result = matchHeader(buildMatchInfo('^(?<cell_name>cell-\\d+)$'), buildRequestWithHeader(name, `dell-4`));

      expect(result).toBe(expected);
    });
  });

  describe('when it matches but there is no capture group', () => {
    it('returns {}', async () => {
      const expected = {};
      const result = matchHeader(buildMatchInfo('^cell-\\d+$'), buildRequestWithHeader(name, `cell-4`));

      expect(result).toEqual(expected);
    });
  });
});

describe('matchQueryString', () => {
  const name = 'force-cell';

  function buildMatchInfo(regex_value: string): RegexMatchInfo {
    return { type: MatchType.QueryString, regex_name: name, regex_value: regex_value };
  }

  it('returns the capture groups', async () => {
    const expected = { cell_name: 'cell-4' };
    const result = matchQueryString(buildMatchInfo('^(?<cell_name>cell-\\d+)$'), buildURLWithSearchParams({ [name]: 'cell-4' }));

    expect(result).toEqual(expected);
  });

  describe('when it does not match', () => {
    it('returns null', async () => {
      const expected = null;
      const result = matchQueryString(buildMatchInfo('^(?<cell_name>cell-\\d+)-.*'), buildURLWithSearchParams({ [name]: 'bell-4' }));

      expect(result).toBe(expected);
    });
  });

  describe('when it matches but there is no capture group', () => {
    it('returns {}', async () => {
      const expected = {};
      const result = matchQueryString(buildMatchInfo('cell-\\d+$'), buildURLWithSearchParams({ [name]: 'cell-4' }));

      expect(result).toEqual(expected);
    });
  });
});

describe('matchPath', () => {
  function buildMatchInfo(regex_value: string): RegexMatchInfo {
    return { type: MatchType.Path, regex_name: '', regex_value: regex_value };
  }

  function buildUrl(path: string): URL {
    return new URL(`https://example.com${path}`);
  }

  it('returns the capture groups', () => {
    const expected = { project_path: 'group/project' };
    const result = matchPath(buildMatchInfo('^/(?<project_path>[^/]+/[^/]+)'), buildUrl('/group/project/issues'));

    expect(result).toEqual(expected);
  });

  describe('when it does not match', () => {
    it('returns null', () => {
      const expected = null;
      const result = matchPath(buildMatchInfo('^/(<project_path>[^/]+/[^/]+)'), buildUrl('/user/profile'));
      expect(result).toBe(expected);
    });
  });

  describe('when it matches but there is no capture group', () => {
    it('returns {}', () => {
      const expected = {};
      const result = matchPath(buildMatchInfo('^/group/project'), buildUrl('/group/project/issues'));

      expect(result).toEqual(expected);
    });
  });
});

describe('matchRequest', () => {
  const cookieName = '_gitlab_session';
  const cookieValue = 'cookie-value';
  const headerKey = 'FoRcE-CelL';
  const headerValue = 'cell-1';
  const searchParamName = 'force-cell';
  const searchParamRegex = '^(?<cell_name>cell-\\d+)$';
  const pathRegexp = '^/test/path$';

  function buildMatchInfo(type: MatchType, regex_name: string, regex_value: string): RegexMatchInfo {
    return { type: type, regex_name: regex_name, regex_value: regex_value };
  }

  function buildHeaderMatchInfo(type: MatchType, name: string, value: string): HeaderMatchInfo {
    return { type: type, name: name, regex_value: value };
  }

  it('uses matchCookie when type is cookie', async () => {
    const expected = {};
    const result = matchRequest(
      buildMatchInfo(MatchType.Cookie, cookieName, cookieValue),
      buildRequestWithCookie(`${cookieName}=${cookieValue}`),
    );

    expect(result).toEqual(expected);
  });

  it('uses matchHeader when type is header', async () => {
    const expected = {};
    const result = matchRequest(
      buildHeaderMatchInfo(MatchType.Header, headerKey, headerValue),
      buildRequestWithHeader(headerKey, headerValue),
    );

    expect(result).toEqual(expected);
  });

  it('uses matchQueryString when type is query_string', async () => {
    const expected = { cell_name: 'cell-1' };
    const result = matchRequest(
      buildMatchInfo(MatchType.QueryString, searchParamName, searchParamRegex),
      buildRequest(buildURLWithSearchParams({ [searchParamName]: 'cell-1' })),
    );

    expect(result).toEqual(expected);
  });

  it('uses matchPath when type is path', async () => {
    const expected = {};
    const result = matchRequest(buildMatchInfo(MatchType.Path, 'path', pathRegexp), buildRequestWithCookie('/test/path'));

    expect(result).toEqual(expected);
  });
});
