import { MatchResult, VersionRuleInfo } from '../../src/rules/types';
import { VersionRule } from '../../src/rules/version';
import { describe, it, expect, beforeEach, vi } from 'vitest';
import { createExecutionContext, env } from 'cloudflare:test';
import { buildSilentLogger } from '../../src/logger';
import { buildRequest } from '../helper';

describe('VersionRule', () => {
  const logger = buildSilentLogger();
  const request = buildRequest('http://example.com');
  const matchResult: MatchResult = {};
  const ctx = createExecutionContext();
  const versionRule = new VersionRule({} as VersionRuleInfo, logger);

  it('should return response with correct Content-Type header', async () => {
    const response = await versionRule.fetch(matchResult, request, env, ctx);

    expect(response.headers.get('Content-Type')).toBe('application/json');
  });

  describe('with env variables', async () => {
    beforeEach(() => {
      vi.spyOn(env, 'CF_VERSION_METADATA', 'get').mockReturnValue({
        id: '12345',
        tag: 'v1.0.0',
        timestamp: '2025-01-01',
      });
    });

    it('should return version metadata from env variables', async () => {
      const response = await versionRule.fetch(matchResult, request, env, ctx);

      expect(response.status).toBe(200);
      const responseBody = await response.json();
      expect(responseBody).toEqual({
        id: '12345',
        tag: 'v1.0.0',
      });
    });
  });

  describe('without env variables', async () => {
    it('should return default metadata if env variables are missing', async () => {
      vi.spyOn(env, 'CF_VERSION_METADATA', 'get').mockReturnValue(undefined);

      const response = await versionRule.fetch(matchResult, request, env, ctx);

      expect(response.status).toBe(200);
      const responseBody = await response.json();
      expect(responseBody).toEqual({ id: 'dev', tag: 'dev' });
    });
  });
});
