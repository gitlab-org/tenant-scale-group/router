import { describe, it, expect } from 'vitest';
import { ClassifyRule } from '../../src/rules/classify';
import { ProxyRule } from '../../src/rules/proxy';
import { VersionRule } from '../../src/rules/version';
import { buildRules } from '../../src/rules/index';
import { buildSilentLogger } from '../../src/logger';

describe('buildRules', () => {
  const logger = buildSilentLogger();

  describe('passthrough', () => {
    it('returns a ProxyRule', async () => {
      const rules = buildRules('passthrough', logger);
      expect(rules.length).toBe(2);
      const [versionRule, passthroughRule] = rules;
      expect(versionRule instanceof VersionRule).toBe(true);
      expect(passthroughRule instanceof ProxyRule).toBe(true);
    });
  });

  describe('firstcell', () => {
    it('returns a ClassifyRule', async () => {
      const rules = buildRules('firstcell', logger);
      const passthrough = rules[0];

      expect(rules.length).toBe(1);
      expect(passthrough instanceof ClassifyRule).toBe(true);
    });
  });

  describe('session_prefix', () => {
    it('returns corresponding rules', async () => {
      const rules = buildRules('session_prefix', logger);
      const [version, classifySession, proxy] = rules;

      expect(rules.length).toBe(3);
      expect(version instanceof VersionRule).toBe(true);
      expect(classifySession instanceof ClassifyRule).toBe(true);
      expect(proxy instanceof ProxyRule).toBe(true);
    });
  });

  describe('session_token', () => {
    it('returns corresponding rules', async () => {
      const rules = buildRules('session_token', logger);

      expect(rules).toHaveLength(10);

      const expectedRules = [
        { instance: VersionRule, id: 'version' },
        { instance: ClassifyRule, id: 'session_token_header_job_token' },
        { instance: ClassifyRule, id: 'session_token_header_private_token_job_token' },
        { instance: ClassifyRule, id: 'session_token_query_string_job_token' },
        { instance: ClassifyRule, id: 'session_token_header_runner_token' },
        { instance: ClassifyRule, id: 'session_token_header_authorization_pat' },
        { instance: ClassifyRule, id: 'session_token_header_private_token_pat' },
        { instance: ClassifyRule, id: 'session_token_query_string_pat' },
        { instance: ClassifyRule, id: 'session_prefix_gitlab_session' },
        { instance: ProxyRule, id: 'session_token_proxy' },
      ];

      expectedRules.forEach((expected, index) => {
        const rule = rules[index];
        expect(rule).toBeInstanceOf(expected.instance);
        expect(rule['ruleInfo']['id']).toBe(expected.id);
      });
    });
  });
});
