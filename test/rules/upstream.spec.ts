import { describe, it, expect } from 'vitest';
import { fetchMock } from 'cloudflare:test';
import { buildRequest } from '../helper';
import { Upstream } from '../../src/rules/upstream';

describe('Upstream', () => {
  describe('#fetch', () => {
    it('modifies the request URL and set the X-Forwarded-Host header', async () => {
      fetchMock
        .get('https://proxied.com')
        .intercept({ path: '/path', headers: { 'x-forwarded-host': 'example.com' } })
        .reply(200, 'proxied response');

      const request = buildRequest('https://example.com/path');
      const response = await new Upstream(request, 'proxied.com').fetch();

      expect(await response.text()).toBe('proxied response');
    });
  });
});
