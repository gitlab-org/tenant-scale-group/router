import { describe, expect, it, vi } from 'vitest';
import {
  assertProxyResponse,
  mockLegacyErrorResponse,
  mockLegacyResponse,
  withProxyHost,
  withRulesConfig,
  workerFetchRequest,
  workerFetchURL,
} from '../helper';
import { fetchMock } from 'cloudflare:test';
import { InvalidTokenPayloadError, InvalidUrlSafeBase64, NotFound } from '../../src/error';
import { Logger } from '../../src/logger';
import { ActionType } from '../../src/rules/types.d';
import { ROUTER_RULE_ACTION_HEADER, ROUTER_RULE_TYPE_HEADER } from '../../src/rules/constants';
import { buildRequest } from '../helper';

describe('with `passthrough` rule', () => {
  withRulesConfig('passthrough');

  describe('when proxy is configured to cell', () => {
    withProxyHost('legacy.example.com');

    it('responds using proxy rule', async () => {
      const request = buildRequest('http://example.com');

      assertProxyResponse(request);
    });

    it('sets correct headers', async () => {
      const headersSpy = vi.fn();

      fetchMock
        .get('http://legacy.example.com')
        .intercept({ path: '/' })
        .reply(200, (request) => {
          headersSpy(request.headers);
          return 'response from legacy';
        });

      const response = await workerFetchURL('http://example.com');
      expect(await response.text()).toBe('response from legacy');

      expect(headersSpy).toHaveBeenCalled();
      const capturedHeaders = headersSpy.mock.calls[0][0];
      expect(capturedHeaders[ROUTER_RULE_ACTION_HEADER.toLowerCase()]).toBe(ActionType.Proxy);
      expect(capturedHeaders[ROUTER_RULE_TYPE_HEADER.toLowerCase()]).toBeUndefined();
    });

    it('logs the matched rule and proxy action', async () => {
      const logSpy = vi.spyOn(Logger.getInstance(), 'info');
      const request = buildRequest('http://example.com');

      mockLegacyResponse(request);
      await workerFetchRequest(request);

      expect(logSpy).toHaveBeenCalledTimes(2);
      expect(logSpy).toHaveBeenCalledWith({ ruleAction: 'proxy', ruleApplied: '0_proxy' });
      expect(logSpy).toHaveBeenCalledWith({ proxyHost: 'legacy.example.com' });
    });

    it('responds 502 when upstream connection lost', async () => {
      const request = buildRequest('http://example.com');

      mockLegacyErrorResponse(request, new Error('Network connection lost.'));

      const response = await workerFetchRequest(request);
      expect(await response.text()).toBe('502 Bad Gateway');
      expect(response.status).toBe(502);
    });

    it('responds 400 Bad Request when there is InvalidUrlSafeBase64 error', async () => {
      const request = buildRequest('http://example.com');

      mockLegacyErrorResponse(request, new InvalidUrlSafeBase64('Invalid URL safe base64:'));

      const response = await workerFetchRequest(request);
      expect(await response.text()).toBe('400 Bad Request');
      expect(response.status).toBe(400);
    });

    it('responds 400 Bad Request when there is InvalidTokenPayloadError error', async () => {
      const request = buildRequest('http://example.com');

      mockLegacyErrorResponse(request, new InvalidTokenPayloadError('Invalid pair: key or value'));

      const response = await workerFetchRequest(request);
      expect(await response.text()).toBe('400 Bad Request');
      expect(response.status).toBe(400);
    });

    it('responds 404 Not Found when there is NotFound error', async () => {
      const request = buildRequest('http://example.com');

      mockLegacyErrorResponse(request, new NotFound('Client Error: Not Found'));

      const response = await workerFetchRequest(request);

      expect(await response.text()).toBe('404 Not Found');
      expect(response.status).toBe(404);
    });
  });

  describe('when proxy is not configured', () => {
    it('returns a Promise.reject', async () => {
      const spy = vi.fn((error) => {
        expect(error.message).toBe('Proxy address is not specified in rule or environment.');
      });
      const promise = workerFetchURL('http://example.com');
      await promise.catch(spy);

      expect(spy).toHaveBeenCalled();
    });
  });
});
