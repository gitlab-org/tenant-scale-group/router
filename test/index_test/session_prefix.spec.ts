import { fetchMock } from 'cloudflare:test';
import { describe, it, expect, vi } from 'vitest';
import {
  buildRequest,
  withRulesConfig,
  withProxyHost,
  withTopologyService,
  workerFetchRequest,
  mockTopologyAndBackend,
  assertProxyResponse,
} from '../helper';
import { CLASSIFY_PATH } from '../../src/topology_service/constants';
import { ROUTER_RULE_ACTION_HEADER, ROUTER_RULE_TYPE_HEADER } from '../../src/rules/constants';
import { ActionType, ClassifyType } from '../../src/rules/types.d';

describe('with `session_prefix` rule', () => {
  withRulesConfig('session_prefix');

  describe('when session cookie matches the prefix', () => {
    withTopologyService('http://ts3.example.com');

    describe('when value matched the format', () => {
      it('sets correct headers', async () => {
        const headersSpy = vi.fn();

        fetchMock
          .get('http://ts3.example.com')
          .intercept({
            path: CLASSIFY_PATH,
            method: 'POST',
            body: JSON.stringify({ type: ClassifyType.SessionPrefix, value: 'cell-135' }),
            headers: { 'Content-Type': 'application/json' },
          })
          .reply(200, { action: 'PROXY', proxy: { address: 'cell-135.example.com' } });

        fetchMock
          .get('http://cell-135.example.com')
          .intercept({ path: '/' })
          .reply(200, (request) => {
            headersSpy(request.headers);
            return 'response from cell-135';
          });

        const request = buildRequest('http://example.com', { cookies: { _gitlab_session: 'cell-135-random-gitlab-session-id' } });

        const response = await workerFetchRequest(request);
        expect(await response.text()).toBe('response from cell-135');

        expect(headersSpy).toHaveBeenCalled();
        const capturedHeaders = headersSpy.mock.calls[0][0];
        expect(capturedHeaders[ROUTER_RULE_ACTION_HEADER.toLowerCase()]).toBe(ActionType.Classify);
        expect(capturedHeaders[ROUTER_RULE_TYPE_HEADER.toLowerCase()]).toBe(ClassifyType.SessionPrefix);
      });
    });

    describe('when cookie name has postfix in the session cookie for local GDK', () => {
      it('matches', async () => {
        const request = buildRequest('http://example.com', {
          cookies: {
            _gitlab_session_ded842a2e07089230946000c0bb89783a57ba4c68fe7fb381b32b146978352b4: 'cell-246-random-gitlab-session-id',
          },
        });

        mockTopologyAndBackend(request, '246', ClassifyType.SessionPrefix);

        const response = await workerFetchRequest(request);
        expect(await response.text()).toBe('response from cell-246.example.com');
      });
    });

    describe('when value does not match the format', () => {
      withProxyHost('legacy.example.com');

      it('responds using proxy rule', async () => {
        const request = buildRequest('http://example.com', { cookies: { _gitlab_session: 'random-gitlab-session-id' } });

        assertProxyResponse(request);
      });
    });
  });

  describe('when nothing matched', () => {
    withProxyHost('legacy.example.com');

    it('responds using proxy rule', async () => {
      const request = buildRequest('http://example.com');

      assertProxyResponse(request);
    });
  });
});
