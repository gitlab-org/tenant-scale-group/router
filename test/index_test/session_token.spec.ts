import { describe, it, expect } from 'vitest';
import { encodeUrlSafeBase64 } from '../../src/utils';
import {
  buildRequest,
  withRulesConfig,
  withProxyHost,
  withTopologyService,
  workerFetchRequest,
  generateRoutableToken,
  assertResponseWithRule,
  assertProxyResponse,
} from '../helper';

describe('with `session_token` rule', () => {
  withRulesConfig('session_token');
  withProxyHost('legacy.example.com');

  describe('when nothing matched', () => {
    it('responds using proxy rule', async () => {
      const request = buildRequest('http://example.com');

      assertProxyResponse(request);
    });
  });

  describe('when a non-routable personal access token is used', () => {
    it('responds using proxy rule', async () => {
      const request = buildRequest('http://example.com/', {
        headers: { 'PRIVATE-TOKEN': 'glpat-DeadBeafLivePork1024' },
      });

      assertProxyResponse(request);
    });
  });

  describe('with Runner Authentication Token', () => {
    withTopologyService('http://ts3.example.com');

    describe('when value matched the format', () => {
      describe('when validation passed', () => {
        describe('with runner token with standard glrt-t3_ prefix', () => {
          it('responds from the classified cell', async () => {
            const cellID = 135;
            const encodedCellID = cellID.toString(36);

            const request = buildRequest('http://example.com/', {
              headers: { 'Private-Token': `glrt-t3_${generateRoutableToken(`r:bytes\nc:${encodedCellID}`)}` },
            });

            await assertResponseWithRule(request, cellID, 'session_token_header_runner_token');
          });
        });

        describe('with runner token without glrt- prefix from runner created via deprecated registration token', () => {
          it('responds from the classified cell', async () => {
            const cellID = 324;
            const encodedCellID = cellID.toString(36);

            const request = buildRequest('http://example.com/', {
              headers: { 'Private-Token': `t2_${generateRoutableToken(`r:bytes\nc:${encodedCellID}`)}` },
            });

            await assertResponseWithRule(request, cellID, 'session_token_header_runner_token');
          });
        });
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { 'Private-Token': `glrt-${generateRoutableToken(`r:bytes\no:777`)}` },
        });

        assertProxyResponse(request);
      });
    });

    describe('when value does not match the format', () => {
      it('responds with 400 Bad Request', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { 'Private-Token': `glrt-${generateRoutableToken('r:bytes\nc:135').replace('.', '-gl.')}` },
        });

        const response = await workerFetchRequest(request);
        expect(await response.text()).toBe('400 Bad Request');
        expect(response.status).toBe(400);
      });
    });
  });

  describe('with PAT from PRIVATE-TOKEN header', () => {
    withTopologyService('http://ts3.example.com');

    describe('when value matched the format', () => {
      it('responds from the classified cell when validation passed', async () => {
        const encodedCellID = (1024).toString(36);
        const request = buildRequest('http://example.com/', {
          headers: { 'PRIVATE-TOKEN': `glpat-${generateRoutableToken(`c:${encodedCellID}`)}` },
        });

        await assertResponseWithRule(request, 1024, 'session_token_header_private_token_pat');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { 'PRIVATE-TOKEN': `glpat-${generateRoutableToken('o:1024')}` },
        });

        assertProxyResponse(request);
      });
    });

    describe('when another token is also provided from query string', () => {
      it('responds based on the token in headers and ignores the token in query string', async () => {
        const encodedCellID = (2049).toString(36);
        const path = '/?private-token=glpat-DeadBeafLivePork1024';
        const request = buildRequest(`http://example.com${path}`, {
          headers: { 'PRIVATE-TOKEN': `glpat-${generateRoutableToken(`c:${encodedCellID}`)}` },
        });

        await assertResponseWithRule(request, 2049, 'session_token_header_private_token_pat');
      });
    });

    describe('when value does not match the format', () => {
      it('responds with 400 Bad Request', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { 'Private-Token': `glpat-${generateRoutableToken('c:1024').replace('.', '-gl.')}` },
        });
        const response = await workerFetchRequest(request);

        expect(await response.text()).toBe('400 Bad Request');
        expect(response.status).toBe(400);
      });
    });
  });

  describe('with PAT from Authorization: Bearer header', () => {
    withTopologyService('http://ts3.example.com');

    describe('when value matched the format', () => {
      it('responds from the classified cell when validation passed', async () => {
        const encodedCellID = (777).toString(36);
        const request = buildRequest('http://example.com/', {
          headers: { Authorization: `Bearer glpat-${generateRoutableToken(`c:${encodedCellID}`)}` },
        });

        await assertResponseWithRule(request, 777, 'session_token_header_authorization_pat');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { Authorization: `Bearer glpat-${generateRoutableToken('o:777')}` },
        });

        assertProxyResponse(request);
      });
    });

    describe('when value does not match the format', () => {
      it('responds with 400 Bad Request', async () => {
        const request = buildRequest('http://example.com/', {
          headers: { Authorization: `Bearer glpat-${generateRoutableToken('c:777').replace('.', '-gl.')}` },
        });
        const response = await workerFetchRequest(request);

        expect(await response.text()).toBe('400 Bad Request');
        expect(response.status).toBe(400);
      });
    });
  });

  describe('with PAT from Query Strings', () => {
    withTopologyService('http://ts3.example.com');

    describe('when value matched the format', () => {
      it('responds from the classified cell when validation passed', async () => {
        const encodedCellID = (420).toString(36);
        const request = buildRequest(`http://example.com?private_token=glpat-${generateRoutableToken(`c:${encodedCellID}`)}`);

        await assertResponseWithRule(request, 420, 'session_token_query_string_pat');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const request = buildRequest(`http://example.com?private_token=glpat-${generateRoutableToken('o:420')}`);

        assertProxyResponse(request);
      });
    });

    describe('when value does not match the format', () => {
      withProxyHost('cell1.example.com');

      it('responds with 400 Bad Request', async () => {
        const path = `/?private_token=glpat-${generateRoutableToken('c:420').replace('.', '-gl.')}`;

        const request = buildRequest(`http://example.com${path}`);
        const response = await workerFetchRequest(request);

        expect(await response.text()).toBe('400 Bad Request');
        expect(response.status).toBe(400);
      });
    });
  });

  describe('with JOB TOKEN', () => {
    withTopologyService('http://topology-service.example.com');

    const encodedCellID = (420).toString(36);
    const routablePayload = { c: encodedCellID, o: '1', p: '2', u: '3', g: '4' };
    const notRoutablePayload = { o: '1', p: '2', u: '3', g: '4' };

    const createJobToken = (payload: Record<string, string>) => {
      return `glcbt-${btoa('header')}.${encodeUrlSafeBase64(JSON.stringify(payload))}.${btoa('signature')}`;
    };

    describe('from JOB-TOKEN headers', () => {
      it('responds from the classified cell when validation passed', async () => {
        const jobToken = createJobToken(routablePayload);
        const request = buildRequest('http://example.com/', { headers: { 'JOB-TOKEN': jobToken } });

        await assertResponseWithRule(request, 420, 'session_token_header_job_token');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const jobToken = createJobToken(notRoutablePayload);
        const request = buildRequest('http://example.com/', { headers: { 'JOB-TOKEN': jobToken } });

        assertProxyResponse(request);
      });
    });

    describe('from PRIVATE-TOKEN headers', () => {
      it('responds from the classified cell when validation passed', async () => {
        const jobToken = createJobToken(routablePayload);
        const request = buildRequest('http://example.com/', { headers: { 'PRIVATE-TOKEN': jobToken } });

        await assertResponseWithRule(request, 420, 'session_token_header_private_token_job_token');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const jobToken = createJobToken(notRoutablePayload);
        const request = buildRequest('http://example.com/', { headers: { 'PRIVATE-TOKEN': jobToken } });

        assertProxyResponse(request);
      });
    });

    describe('from Query Strings', () => {
      it('responds from the classified cell when validation passed', async () => {
        const jobToken = createJobToken(routablePayload);
        const request = buildRequest(`http://example.com?job_token=${jobToken}`);

        await assertResponseWithRule(request, 420, 'session_token_query_string_job_token');
      });

      it('responds using proxy rule when validation does not pass', async () => {
        const jobToken = createJobToken(notRoutablePayload);
        const request = buildRequest(`http://example.com?job_token=${jobToken}`);

        assertProxyResponse(request);
      });
    });
  });
});
