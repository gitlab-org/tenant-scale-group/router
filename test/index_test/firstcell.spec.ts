import { fetchMock } from 'cloudflare:test';
import { describe, it, expect, vi } from 'vitest';
import { withRulesConfig, withTopologyService, workerFetchURL } from '../helper';
import { CLASSIFY_PATH } from '../../src/topology_service/constants';
import { ROUTER_RULE_ACTION_HEADER, ROUTER_RULE_TYPE_HEADER } from '../../src/rules/constants';
import { Logger } from '../../src/logger';
import { ActionType, ClassifyType } from '../../src/rules/types.d';
import * as utils from '../../src/utils';

describe('with `firstcell` rule', () => {
  withRulesConfig('firstcell');

  describe('when Topology Service is not configured', () => {
    it('returns a Promise.reject', async () => {
      const spy = vi.fn((error) => {
        expect(error.message).toBe('Topology Service is not configured to perform classify action');
      });
      const promise = workerFetchURL('http://example.com');
      await promise.catch(spy);

      expect(spy).toHaveBeenCalled();
    });
  });

  describe('when Topology Service is configured', () => {
    withTopologyService('http://topology-service.example.com');

    it('should fetch and cache response if not cached', async () => {
      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .reply(200, { action: 'PROXY', proxy: { address: 'cell1.example.com' } });

      fetchMock
        .get('http://cell1.example.com')
        .intercept({ path: '/', headers: { 'x-forwarded-host': 'example.com' } })
        .reply(200, 'response from cell1');

      const cachesDefaultMatchSpy = vi.spyOn(caches.default, 'match');
      const cachesDefaultPutSpy = vi.spyOn(caches.default, 'put');
      const response = await workerFetchURL('http://example.com');

      expect(await response.text()).toBe('response from cell1');
      expect(cachesDefaultMatchSpy).toHaveBeenCalledTimes(1);
      expect(cachesDefaultPutSpy).toHaveBeenCalledTimes(1);
    });

    it('sets correct headers and logs the action', async () => {
      const headersSpy = vi.fn();
      const logSpy = vi.spyOn(Logger.getInstance(), 'info');

      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .reply(200, { action: 'PROXY', proxy: { address: 'cell1.example.com' } });

      fetchMock
        .get('http://cell1.example.com')
        .intercept({ path: '/' })
        .reply(200, (request) => {
          headersSpy(request.headers);
          return 'response from cell1';
        });

      const response = await workerFetchURL('http://example.com');
      expect(await response.text()).toBe('response from cell1');

      expect(headersSpy).toHaveBeenCalled();
      const capturedHeaders = headersSpy.mock.calls[0][0];
      expect(capturedHeaders[ROUTER_RULE_ACTION_HEADER.toLowerCase()]).toBe(ActionType.Classify);
      expect(capturedHeaders[ROUTER_RULE_TYPE_HEADER.toLowerCase()]).toBe(ClassifyType.FirstCell);

      expect(logSpy).toHaveBeenCalledTimes(4);
      expect(logSpy).toHaveBeenCalledWith({
        cache: 'miss',
        cacheFetchDurationMs: expect.any(Number),
        source: 'http://topology-service.example.com/v1/classify',
      });
      expect(logSpy).toHaveBeenCalledWith({ ruleAction: 'classify', ruleApplied: '0_first_cell' });
      expect(logSpy).toHaveBeenCalledWith({ proxyHost: 'cell1.example.com' });
      expect(logSpy).toHaveBeenCalledWith({
        source: 'http://topology-service.example.com/v1/classify',
        fetchDurationMs: expect.any(Number),
        fetchStatus: 200,
        attempt: 1,
      });
    });

    it('retries request 3 times and responds 503 when Topology Service responds 503', async () => {
      vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());

      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .reply(503, { 503: 'Service Unavailable' })
        .times(3);

      const promise = await workerFetchURL('http://example.com');

      expect(promise.statusText).toBe('Service Unavailable');
      expect(promise.status).toBe(503);
    });

    it('raises an exception for 4xx Client Errors', async () => {
      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .reply(401, { 401: 'Unauthorized' });

      const spy = vi.fn((error) => {
        expect(error.message).toBe('Client Error: Unauthorized');
      });
      const promise = workerFetchURL('http://example.com');
      await promise.catch(spy);

      expect(spy).toHaveBeenCalled();
    });

    it('retries request 3 times and responds 503 when Topology Service has timeout error', async () => {
      vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());

      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .replyWithError(new DOMException('The operation timed out.', 'TimeoutError'))
        .times(3);

      const promise = await workerFetchURL('http://example.com');

      expect(promise.statusText).toBe('Service Unavailable');
      expect(promise.status).toBe(503);
    });

    it('raises an exception for non-OK reponses', async () => {
      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .reply(300, { 300: 'Multiple Choices' });

      const spy = vi.fn((error) => {
        expect(error.message).toBe('Service is not reachable. Multiple Choices');
      });
      const promise = workerFetchURL('http://example.com');
      await promise.catch(spy);

      expect(spy).toHaveBeenCalled();
    });

    it('retries request 3 times and responds 503 when Topology Service cannot be reached', async () => {
      vi.spyOn(utils, 'sleepPromise').mockImplementation(() => Promise.resolve());

      fetchMock
        .get('http://topology-service.example.com')
        .intercept({
          path: CLASSIFY_PATH,
          method: 'POST',
          body: JSON.stringify({ type: ClassifyType.FirstCell }),
          headers: { 'Content-Type': 'application/json' },
        })
        .replyWithError(new Error('Network connection lost.'))
        .times(3);

      const promise = await workerFetchURL('http://example.com');

      expect(promise.statusText).toBe('Service Unavailable');
      expect(promise.status).toBe(503);
    });

    it('should return cached response if available', async () => {
      const logSpy = vi.spyOn(Logger.getInstance(), 'info');
      const cachedResponse = new Response(JSON.stringify({ action: 'PROXY', proxy: { address: 'cached.example.com' } }), {
        headers: { 'Content-Type': 'application/json' },
      });

      const cachesDefaultMatchSpy = vi.spyOn(caches.default, 'match');
      cachesDefaultMatchSpy.mockResolvedValue(cachedResponse);

      fetchMock
        .get('http://cached.example.com')
        .intercept({ path: '/', headers: { 'x-forwarded-host': 'example.com' } })
        .reply(200, 'response from cell1');

      const cachesDefaultPutSpy = vi.spyOn(caches.default, 'put');
      const response = await workerFetchURL('http://example.com');

      expect(await response.text()).toBe('response from cell1');
      expect(cachesDefaultMatchSpy).toHaveBeenCalledTimes(1);
      expect(cachesDefaultPutSpy).not.toBeCalled();

      expect(logSpy).toHaveBeenCalledTimes(4);
      expect(logSpy).toHaveBeenCalledWith({
        cache: 'hit',
        cacheFetchDurationMs: expect.any(Number),
        source: 'http://topology-service.example.com/v1/classify',
      });
      expect(logSpy).toHaveBeenCalledWith({ ruleAction: 'classify', ruleApplied: '0_first_cell' });
      expect(logSpy).toHaveBeenCalledWith({ proxyHost: 'cached.example.com' });
      expect(logSpy).toHaveBeenCalledWith({
        source: '',
        fetchDurationMs: expect.any(Number),
        fetchStatus: response.status,
        attempt: 1,
      });
    });
  });
});
