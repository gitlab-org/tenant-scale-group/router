# GitLab HTTP Router

## Documentation

- [CONTRIBUTING.md](CONTRIBUTING.md)
- [Style Guide](docs/style-guide.md)
- [Configuration](docs/config.md)
- [Deployment](docs/deployment.md)
- [Development](docs/development.md)
- [Rules](docs/rules.md)
- [Members](docs/members.md)
- [Debugging](docs/debugging.md)
- [Logging](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/http-router/logging.md)

## Relevant Blueprint Pages

- [HTTP Routing Service](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/http_routing_service/)
- [Cells ADR 010: HTTP Router uses static rules and HTTP-based caching mechanism](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/decisions/010_http_router_rules_and_cache/)
