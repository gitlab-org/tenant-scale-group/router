// This is a fake upstream server that is meant
// run integration tests for the gitlab-http-router against it

import * as http from 'http';

const args = process.argv.slice(2);
const PORT: number = args.length > 0 ? parseInt(args[0]) : 4000;

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });

  if (req.url === '/') {
    res.end('Hello World!');
  }
});

server.listen(PORT, () => {});
