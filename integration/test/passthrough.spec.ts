import { describe, it, expect, beforeAll, afterAll } from 'vitest';
import { fetchUrl } from '../src/helpers';
import { startProcessInDirectory, waitForServersReady, ManagedProcess } from '../src/processUtils';
import { startWrangler } from '../src/wrangler';

describe('passthrough mode', () => {
  let wrangler: ManagedProcess;
  let upstreamProxy: ManagedProcess;

  const ip = 'localhost';
  const port = 8001;
  const baseUrl = `http://${ip}:${port}`;
  const upstreamProxyPort = 4100;
  const gitlabRulesConfig = 'passthrough';

  beforeAll(async () => {
    wrangler = startWrangler(gitlabRulesConfig, ip, port, upstreamProxyPort);

    const upstreamDirectory = './test/server/';
    upstreamProxy = startProcessInDirectory('npx', ['tsx', 'server.ts', upstreamProxyPort.toString()], upstreamDirectory, {
      shell: true,
    });

    // Timeout to make sure that the services are up and running
    await waitForServersReady([wrangler, upstreamProxy]);
  });

  afterAll(async () => {
    await upstreamProxy.stop();
    await wrangler.stop();
  });

  it('should return 200 with version on the version path', async () => {
    const path = '/-/http_router/version';
    const response = await fetchUrl(`${baseUrl}${path}`);
    const data: object = await response.json();
    expect(Object.keys(data)).toEqual(expect.arrayContaining(['id', 'tag']));
    expect(response.status).toBe(200);
  });

  it('should return 200 status code for root', async () => {
    const path = '/';
    const response = await fetchUrl(`${baseUrl}${path}`);
    const data = await response.text();
    expect(data).toBe('Hello World!');
    expect(response.status).toBe(200);
  });
});
