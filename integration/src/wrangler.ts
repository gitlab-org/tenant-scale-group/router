import { startProcessInDirectory } from './processUtils';

export function startWrangler(gitlabRulesConfig: string, ip: string, port: number, proxyPort: number) {
  const projectDir = '../';
  return startProcessInDirectory(
    'npm',
    [
      'run',
      'dev',
      '--',
      '-c',
      'wrangler.toml',
      '--local-protocol',
      'http',
      '--ip',
      ip,
      '--port',
      port.toString(),
      '--var',
      `GITLAB_PROXY_HOST:${ip}:${proxyPort}`,
      '--var',
      `GITLAB_RULES_CONFIG:${gitlabRulesConfig}`,
    ],
    projectDir,
    { shell: true },
  );
}
