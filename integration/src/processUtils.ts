import { spawn, SpawnOptions, ChildProcess } from 'child_process';
import path from 'path';

/**
 * Interface for process execution results
 */
export interface ProcessResult {
  stdout: string;
  stderr: string;
  exitCode: number | null;
}

/**
 * Interface for a managed process that can be controlled
 */
export interface ManagedProcess {
  /** The underlying child process instance */
  process: ChildProcess;
  /** Promise that resolves when the process completes */
  completed: Promise<ProcessResult>;
  /** Function to stop the process */
  stop: (signal?: NodeJS.Signals) => Promise<void>;
}

/**
 * Starts a process from a specific directory with the ability to stop it
 * @param command The command to execute
 * @param args Arguments to pass to the command
 * @param directory The directory to start the process from
 * @param options Additional spawn options
 * @returns A ManagedProcess object that allows controlling the process
 */
export function startProcessInDirectory(
  command: string,
  args: string[] = [],
  directory: string,
  options: Omit<SpawnOptions, 'cwd'> = {},
): ManagedProcess {
  // Resolve the directory path
  const workingDir = path.resolve(directory);

  // Configure spawn options with the working directory
  const spawnOptions: SpawnOptions = {
    ...options,
    detached: false,
    cwd: workingDir,
  };

  // Spawn the process
  const childProcess = spawn(command, args, spawnOptions);

  let stdout = '';
  let stderr = '';

  // Collect stdout data
  childProcess.stdout?.on('data', (data) => {
    stdout += data.toString();
  });

  // Collect stderr data
  childProcess.stderr?.on('data', (data) => {
    stderr += data.toString();
  });

  // Create the completion promise
  const completedPromise = new Promise<ProcessResult>((resolve, reject) => {
    // Handle process completion
    childProcess.on('close', (exitCode) => {
      resolve({
        stdout,
        stderr,
        exitCode,
      });
    });

    // Handle process errors
    childProcess.on('error', (err) => {
      reject(new Error(`Failed to start process: ${err.message}`));
    });
  });

  // Create stop function
  const stopProcess = async (): Promise<void> => {
    if (childProcess.killed) {
      return;
    }

    return new Promise<void>((resolve) => {
      // Set up one-time handler for the close event
      childProcess.once('close', () => {
        resolve();
      });

      try {
        childProcess.stdout?.destroy();
        childProcess.stderr?.destroy();
        childProcess.removeAllListeners();
        childProcess.kill('SIGINT');
      } finally {
        resolve();
      }

      setTimeout(() => {
        if (!childProcess.killed && childProcess.pid) {
          try {
            process.kill(-childProcess.pid, 'SIGTERM');
          } finally {
            resolve();
          }
        }
      }, 3000);

      setTimeout(() => {
        if (!childProcess.killed && childProcess.pid) {
          try {
            process.kill(-childProcess.pid, 'SIGKILL');
          } finally {
            resolve();
          }
        }
      }, 5000);
    });
  };

  // Return the managed process object
  return {
    process: childProcess,
    completed: completedPromise,
    stop: stopProcess,
  };
}

export async function waitForServersReady(processes: ManagedProcess[]) {
  // Just wait for 6 seconds and assume they're ready as for now
  if (processes.length > 0) {
    return new Promise((f) => setTimeout(f, 6000));
  }
}
