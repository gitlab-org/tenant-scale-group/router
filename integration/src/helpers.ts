export async function fetchUrl(url: string): Promise<Response> {
  try {
    const response = await fetch(url);
    return response;
  } catch (error) {
    throw new Error(`Failed to fetch homepage: ${error}`);
  }
}
