## What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## References

**Please include [cross links](https://handbook.gitlab.com/handbook/communication/#start-with-a-merge-request:~:text=Cross%20link%20issues,alternate%20if%20duplicate) to any resources that are relevant to this MR**.
This will give reviewers and future readers helpful context to give an efficient review of the changes introduced.

-

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

## MR acceptance checklist

**Please evaluate this MR against the [MR acceptance checklist](https://docs.gitlab.com/ee/development/code_review.html#acceptance-checklist).**
It helps you analyze changes to reduce risks in quality, performance, reliability, security, and maintainability.

<!-- template sourced from https://gitlab.com/gitlab-org/cells/http-router/-/blob/master/.gitlab/merge_request_templates/Default.md -->

<!-- It's inspired from https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Default.md -->

/assign me
