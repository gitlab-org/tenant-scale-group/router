# Session Prefix

The [session prefix](../../config/ruleset/session_prefix.json) consists of 3 main rules:

1. `3_version`: Shows the version of the HTTP Router it's running.
   During deployments this endpoint is used to ensure the deployed HTTP Router version is available.
2. `1_session_prefix_gitlab_session`: Looks at the `_gitlab_session` cookie and if it matches the pattern [`^(?<cell_name>cell-\\d+)-.*`](https://gitlab.com/gitlab-org/cells/http-router/-/blob/7d0b5ed701323a3002d1b08695562d702f48c647/config/ruleset/session_prefix.json#L16)
   (example: cell-3-ece7c168aa88), classifies the value using Topology Service.
   Topology Service responds with the Cell domain to proxy the request.

   ![session prefix demo](../../imgs/session-prefix.png)

   [source](https://excalidraw.com/#json=z7-ihTQ69trj5vdpXZ-7V,k0NtksWZMRdaR-lHoH3JMQ)

3. `0_session_prefix_proxy`: The default route - if nothing matches, proxies to the Legacy Cell in passthrough mode.

## Force Login to a Specific Cell

`1_session_prefix_gitlab_session` is used to route requests to a Cell whether you are logged in or logged out.
To log into a cell, use your developer tools to update the existing `_gitlab_session`.
Adding `cell-$ID-` prefix to the existing session, for example from `ece7c168aa8` to `cell-3-ece7c168aa88`.
Every browser does this differently, but in Chrome developer tools, go to `Developer Tools > Application > Storage > Cookies`.

![image of how to update session prefix](../../imgs/update-gitlab-session-cookie.png)

Make sure you have only a single `_gitlab_session` cookie specified, as multiple cookies can result in failed logins like we saw in <https://gitlab.com/gitlab-com/gl-infra/tenant-scale/cells-infrastructure/team/-/issues/4#note_2325201134>
