# Routing rules

Routing rules define how to decode requests, identify classification keys, and make routing decisions. These rules are
static and established prior to the HTTP Router deployment. They are specified as a JSON document, outlining a
sequential series of operations.

Each rule should have a unique id, and follow our [style guide](./../style-guide.md#naming-conventions-for-rules)

## Example

```json
[
  {
    "id": "first_cell",
    "action": "classify",
    "classify": {
      "type": "FIRST_CELL"
    }
  },
  {
    "id": "proxy",
    "action": "proxy",
    "proxy": {
      "address": "cell-1.example.com"
    }
  }
]
```

The `action` can be:

- `classify`, indicating that the Topology Service should be used for dynamic classification.
- `proxy`, indicating a passthrough to the fixed host specified in the HTTP Router configuration, unless a
  different `proxy` address is provided. Typically, this fixed host would be `Cell 1` in a cluster.

The Routing Service requires `GITLAB_RULES_CONFIG` environment variable to be valid. Currently, we're
supporting [passthrough](../../config/ruleset/passthrough.json), [firstcell](../../config/ruleset/firstcell.json), [session_prefix](../../config/ruleset/session_prefix.json), and [session_token](../../config/ruleset/session_token.json) rule sets.

For more information, visit the [Cells: HTTP Routing Service](https://docs.gitlab.com/ee/architecture/blueprints/cells/http_routing_service.html#routing-rules) design document.
