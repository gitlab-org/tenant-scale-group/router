# Session Token

The [session token](../../config/ruleset/session_token.json) consists of 10 main rules and serves as the successor to the [session_prefix](../../config/ruleset/session_prefix.json) rule. These rules handle authentication and routing for different types of requests.

## Rule Overview

1. `version`: Shows the version of the HTTP Router it's running. During deployments this endpoint is used to ensure the deployed HTTP Router version is available.

2. **CI Token Rules**:
   CI job token is matching with: [`^glcbt-(?<headers>[0-9A-Za-z_-]+)\\.(?<payload>[0-9A-Za-z_-]+)\\.(?<signature>[0-9A-Za-z_-]+)$`](https://gitlab.com/gitlab-org/cells/http-router/-/blob/b227cceddcf45d5ade6a57943d60c70b6fe48e6f/config/ruleset/session_token.json#L16) in the following rules:
   - `session_token_header_job_token`: Processes in `JOB-TOKEN` header
   - `session_token_header_private_token_job_token`: Processes in `PRIVATE-TOKEN` headers
   - `session_token_query_string_job_token`: Processes in `job_token` query parameter

3. `session_token_header_runner_token`: Processes runner tokens in `PRIVATE-TOKEN` header with [`^(glrt-)?t\\d+_(?<payload>[0-9A-Za-z_-]{27,300})\\.(?<payload_length>[0-9a-z]{2})[0-9a-z]{7}$`](https://gitlab.com/gitlab-org/cells/http-router/-/blob/b227cceddcf45d5ade6a57943d60c70b6fe48e6f/config/ruleset/session_token.json#L91) format

4. **Personal Access Token Authentication Rules**:
   Personal access token authentication matching with: [`^(?<prefix_and_payload>[0-9A-Za-z_-]{27,320})\\.(?<payload_length>[0-9a-z]{2})[0-9a-z]{7}$`](https://gitlab.com/gitlab-org/cells/http-router/-/blob/b227cceddcf45d5ade6a57943d60c70b6fe48e6f/config/ruleset/session_token.json#L117) in the following rules:
   - `session_token_header_authorization_pat`: Processes `Bearer` tokens in `Authorization` header
   - `session_token_header_private_token_pat`: Processes PATs in `PRIVATE-TOKEN` header
   - `session_token_query_string_pat`: Processes PATs in `private_token` query parameter

5. `session_prefix_gitlab_session`: Processes `_gitlab_session` cookie with [`^(?<cell_name>cell-\\d+)-.*`](https://gitlab.com/gitlab-org/cells/http-router/-/blob/b227cceddcf45d5ade6a57943d60c70b6fe48e6f/config/ruleset/session_token.json#L195) format
6. `session_token_proxy`: The default route - if nothing matches, proxies to the Legacy Cell in passthrough mode.

   ![session prefix demo](../../imgs/session-prefix.png)

   [source](https://excalidraw.com/#json=z7-ihTQ69trj5vdpXZ-7V,k0NtksWZMRdaR-lHoH3JMQ)

## Token Processing Flow

For all token-based rules, the process follows this pattern:

1. Match the token against a defined regex pattern
2. Decode using `Base64` to extract payload
3. Validate required information is present
4. Classify request using `CELL_ID` via Topology Service
5. Proxy to the appropriate Cell based on Topology Service response

For session-based routing:

   ![session token demo](../../imgs/session-token.png)

   [source](https://excalidraw.com/#json=EgCAqEUTF1lENfg8OAA9i,ssAq3l1zsl_poR9z34njRg)

For more details on authentication methods, see the [REST API authentication](https://docs.gitlab.com/api/rest/authentication/) documentation.
