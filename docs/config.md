# The `wrangler.toml` file

| Section               | Type       | Required | Description                                                                                                                        |
| --------------------- | ---------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| `name`                | `string`   | true     | The name of your Worker. Alphanumeric characters (`a`,`b`,`c`, etc.) and dashes (`-`) only. Do not use underscores (`_`).          |
| `main`                | `string`   | true     | The path to the entrypoint of your Worker that will be executed. For example: `./src/index.ts.`                                    |
| `compatibility_date`  | `string`   | true     | A date in the form `yyyy-mm-dd`, which will be used to determine which version of the Workers runtime is used.                     |
| `compatibility_flags` | `string[]` | false    | A list of flags that enable features from upcoming features of the Workers runtime, usually used together with compatibility_date. |

## Environment variables

Environment variables are a type of binding that allow you to attach text strings or JSON values to your Worker.

Example:

```ruby
name = "my-worker-dev"

[vars]
GITLAB_PROXY_HOST = "example.com"
GITLAB_TOPOLOGY_SERVICE_URL = "https://topology-service-api.dev.example"
GITLAB_TOPOLOGY_SERVICE_TIMEOUT_MS = 500
GITLAB_RULES_CONFIG = "passthrough"
```

## Routing rules

The following are valid `GITLAB_RULES_CONFIG` values:

- **passthrough**: a routing rule performing passthrough to host defined by `GITLAB_PROXY_HOST`. Requires `GITLAB_PROXY_HOST` to be configured.
- **firstcell**: a routing rule performing dynamic classification to route to the `FirstCell` defined by Topology Service. Requires `GITLAB_TOPOLOGY_SERVICE_URL` to be configured.
- **session_prefix**: a routing rule performing `cookie` matching on the request to match a cell's `session_prefix` attribute as specified in the configuration. If no match is found, it performs passthrough using the proxy rule.
- **session_token**: a routing rule that inspects the request for a token to determine the appropriate cell. The token can be located in headers or query string. If a match is found, the request is routed accordingly; otherwise, it goes through `session_prefix` as described above.

See the [Rules](./rules/index.md) file for routing rules information, description of rules engine, syntax supported and processing behavior.
