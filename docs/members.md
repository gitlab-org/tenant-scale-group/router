# Members

## Add maintainers

This repository uses a [`tenant-scale.yaml`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/main/data/teams/tenant-scale.yaml) file to manage maintainers.

Prerequisites:

- You must have the necessary permissions to modify the [`tenant-scale.yaml`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/main/data/teams/tenant-scale.yaml) file.

To add a maintainer, in the [`tenant-scale.yaml`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/main/data/teams/tenant-scale.yaml) file, append the new maintainer's username to the `http_router_maintainers` section.

Example:

```yaml
http_router_maintainers:
  - sxuereb
  - bmarjanovic
  - tkuah
  - manojmj
  - new_maintainer_username
```
