# Style Guide

These are a set of rules and best practices that we follow in the HTTP Router code base.

## Use Logger Instance

All logging operations must use the [Logger](../src/logger.ts) instance instead of native console methods.
This ensures consistent logging behavior and centralized log management.

```js
// ✅ Use Logger instance
Logger.info({ msg: 'Request received' });
Logger.info({ endpoint: '/users', method: 'GET' });
Logger.info({ error: 'Failed to connect', code: 'CONN_ERROR' });

// ❌ Don't use console methods
console.log('Request received');
console.info({ endpoint: '/users' });
console.info(new Error('Failed to connect'));
```

## Structured Logging

Always use structured JSON objects for logging. This makes logs easier to parse, filter, and analyze.

```js
// ✅ Use structured objects with descriptive keys
Logger.info({
  msg: 'User authentication failed',
  userId: '123',
  reason: 'invalid_credentials',
});

Logger.info({
  msg: 'Database connection failed',
  attempt: 3,
  nextRetryMs: 5000,
});

// ❌ Avoid string-only logs
Logger.info('User authentication failed');

// ❌ Avoid mixing strings and objects
Logger.info('Database connection failed', { attempt: 3 });
```

## Naming Conventions for Rules

When naming rules, avoid including sequential numbers in the rule names unless they serve a clear and necessary purpose. Rules are inherently ordered, and adding numbers can introduce unnecessary complexity and maintenance challenges, especially when rules are modified, removed, or reordered.

```js
// ✅ Use rule names that clearly describe their purpose
{
  "id": "version",
},
{
  "id": "session_token_header_runner_token",
},
...
{
  "id": "session_prefix_gitlab_session",
},
{
  "id": "session_token_proxy",
}

// ❌ Avoid using sequential numbers in the rule names
{
  "id": "3_version",
},
...
{
  "id": "0_session_prefix_proxy",
}
```
