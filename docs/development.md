# Development

## Using GDK

The GitLab HTTP Router is already part of the GDK and is automatically
configured by GDK, which also uses the `wrangler.toml` config in this
project, except for the following 3 environment variables:

* `GITLAB_RULES_CONFIG`
* `GITLAB_PROXY_HOST`
* `GITLAB_TOPOLOGY_SERVICE_URL`

The above 3 environment variables will be passed by GDK from the command line
running the server. To configure those values, set them in your `gdk.yml`
under the config `gitlab_http_router`. Setting them in the config file
`wrangler.toml` will be ignored, because GDK will pass them as command line
arguments that will override the `wrangler.toml` config.

### Setting up local interface

Depending on your GDK configurations, you might need to set up
[local interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#local-interface).

## Running alone without using GDK

If you want to run without GDK, you can follow with the following instructions:

### Setting up your development environment

1. Install `Node.js`.
1. Install `npm`.
1. Clone this repository:

   ```sh
   git clone https://gitlab.com/gitlab-org/cells/http-router.git
   ```

### Running your development environment

1. Create a file named `.dev.vars` with the following:
   (update the values based on your configuration)

   ```
   GITLAB_RULES_CONFIG="session_prefix"
   GITLAB_PROXY_HOST="gdk.test:3000"
   GITLAB_TOPOLOGY_SERVICE_URL="http://gdk.test:9096"
   ```

1. Start the router with:

   ```sh
   npm run dev
   ```

1. To run the unit tests:

   ```sh
   npm test
   ```

1. Integration tests are isolated into their own directory `integration`. To run them:

   ```sh
   cd integration
   npm test
   ```

## Updating your development environment

1. To update, `git pull` on the `main` branch:

   ```sh
   git checkout main
   git pull
   ```
