# Debugging

## Debugging failed pipeline of main branch

When we merge changes into the `main` branch of the project, a [new deployment](./development.md) is triggered. This deployment process initiates multiple downstream pipelines on the OPS mirror. We have a `check_remote` job in the canonical repository that continuously tracks the status of that pipeline on OPS.

The status of the `check_remote` job depends on the pipeline running on the mirror. By reviewing the logs of the `check_remote` job, you can determine the cause of the failure. Currently, there are two common failure patterns, described below:

### Connectivity Issue

The `check_remote` job may fail to connect to the mirror to track the mirrored pipeline, resulting in the error: `read: connection reset by peer.`

This issue is intermittent, so you should simply restart or retry the failed job.

### Failing pipeline on mirror

If the logs indicate that the pipeline failed, they will include a link to the failing pipeline on the OPS instance.

![fail check remote job example](../imgs/fail-check-remote.png)

Follow the pipeline link to the OPS instance. Most of the time, the `deploy-worker` job is the one failing. This job triggers a downstream pipeline on [http-router-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer).

![fail deploy-worker job example](../imgs/fail-mirror-pipeline.png)

Within that pipeline, the `test-worker-gstg` job is often the one that fails. This is yet another downstream pipeline.

![full pipeline](../imgs/full-downstream-pipeline.png)

In any of these cases, the solution is to restart or retry the lowest-level downstream pipeline that failed. Once it passes, retry the main `check_remote` job on canonical repository, and it should succeed.
