# Deployment

The Deployment happens on every push to main by the [http-router-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer) project.

With every commit to the `main` branch, changes are pushed to the [mirrored repository](https://ops.gitlab.net/gitlab-org/cells/http-router) on the `ops` instance. This mirror triggers a [downstream pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines) on the [http-router-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer) `ops` mirror, which then deploys the worker to Cloudflare.

Additionally, the [woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse) job will post a comment on the merged MR, providing a link to the pipeline running on the `ops` instance.

See the diagram below:
![Project Diagram](../imgs/project-workflow.png)

[source](https://excalidraw.com/#json=ZQIZo-x0iUbagqlZteKcJ,LeRKmdRL7FYMoFNlZQtVVQ)

## Gradual Deployments

When we deploy a new commit from the `main` branch, we don’t roll it out to 100% of requests immediately.
We use Cloudflare's [Gradual Deployments](https://developers.cloudflare.com/workers/configuration/versions-and-deployments/gradual-deployments/),
where we shift a `%` of traffic to the new version incrementally, where each increment is at a higher percentage on the new version.

We can see an example of this in <https://ops.gitlab.net/gitlab-com/gl-infra/cells/http-router-deployer/-/jobs/15920407>.
This job deployed a new version of HTTP Router,
starting with enabling the new version by `5%` and the current/old version running in production to `95%`.
The increments are [hard coded](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer/-/blob/47e229366f79f390d1660c579537b678c9817a49/scripts/deploy-worker.sh#L41)
and is meant to follow Runway's [`regional_rollout`](https://docs.runway.gitlab.com/guides/deployment-strategy/#available-deployment-strategies) strategy.

![Gradual Deployment Demo](../imgs/gradual-deployment.png)
[source](https://excalidraw.com/#json=awWzOFAZBEIZA-kOIdk-j,MoUQUZPH7qzJmqdKF-MKmg)

## Rollback

There will be a situation where a new version of the HTTP Router is either causing a performance regression, or a bug.
We can easily rollback the version of HTTP router, and we have a few ways to achieve this:

1. Git revert the bad commit: If we know which commit is causing this problem, we just revert that commit and deploy that normally.
   This should be the preferred way of doing a rollback, since it keeps `main` in sync with what is running in production.
1. Run rollback job: If reverting the commit is too slow for example during an S1 incident, we have a `rollback` job in GitLab CI in the deployment pipeline.
   There could be an ongoing [Gradual deployment](#gradual-deployments) that is causing the S1 incident.
   We need to cancel any active deployments and then press the `rollback` job for the environment to rollback the old version quickly.
   You can find the active deployments [here](https://ops.gitlab.net/gitlab-com/gl-infra/cells/http-router-deployer/-/pipelines).

   ![Rollback](../imgs/rollback.gif)
