#!/usr/bin/env bash
set -euo pipefail

LINES=$(find src -name '*.ts' -or -name '*.js' | xargs grep -v '^\s*$' | wc -l)
THRESHOLD=1000
SOFT_THRESHOLD=$((THRESHOLD * 80 / 100))

echo "The HTTP Router is currently using $LINES out of $THRESHOLD lines."

if [[ $LINES -gt $THRESHOLD ]]; then
  cat <<EOF
The complexity of the HTTP Router exceeds a hard limit of $THRESHOLD lines.

The $THRESHOLD is enforced to ensure that the HTTP Router is simple,
does not contain business and can easily be rewritten from platform-specific
implementation.

Change your implementation or reduce complexity of other parts of the project
to adhere to the enforced limit.
EOF
  exit 1
elif [[ $LINES -gt $SOFT_THRESHOLD ]]; then
  cat <<EOF
Warning: The HTTP Router is approaching the hard limit with $LINES lines, which is more than 80% of the $THRESHOLD lines threshold.

Consider reviewing your implementation to ensure it remains simple and maintainable.
EOF
  exit 2
fi
